package dk.aau.sw9.examples.reflection;

import org.apache.http.client.methods.HttpGet;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends Activity {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        Button btn = (Button)findViewById(R.id.button1);
        btn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				SimpleInspections simpleInspections = new SimpleInspections();
				
				HttpGet hg = new HttpGet("http://vup.dk");
                Toast.makeText(getApplicationContext(), hg.getRequestLine().toString(), Toast.LENGTH_LONG);
                
				StringBuilder inspectResults = new StringBuilder();
				
				//Get class name(s)
				String className = simpleInspections.getHardcoded() == simpleInspections.getCurrent() ? simpleInspections.getCurrent() : "Hardcoded != dynamic";
				inspectResults.append("Class name: " + className + "\n");

				//List constructors
				inspectResults.append("List of constructors: \n");
				inspectResults.append(simpleInspections.getConstructors() + "\n");
				
				Toast.makeText(getApplicationContext(), inspectResults, Toast.LENGTH_LONG).show();
				inspectResults = new StringBuilder();
				
				//Get fields
				inspectResults.append("Fields: \n");
				inspectResults.append(simpleInspections.getFields() + "\n");
				
				//Get specific field
				inspectResults.append("Specific field: \n");
				inspectResults.append(simpleInspections.getPublicField() + "\n");
				
				Toast.makeText(getApplicationContext(), inspectResults, Toast.LENGTH_LONG).show();
				inspectResults = new StringBuilder();
				
				//Get methods
				inspectResults.append("Methods: \n");
				inspectResults.append(simpleInspections.getMethods() + "\n");
				
				//Get specific field
				inspectResults.append("Specific method: \n");
				inspectResults.append(simpleInspections.getSpecificMethod() + "\n");
				
				Toast.makeText(getApplicationContext(), inspectResults, Toast.LENGTH_LONG).show();
				inspectResults = new StringBuilder();
				
				//Call method
				inspectResults.append("Method result: \n");
				inspectResults.append(simpleInspections.callMethod() + "\n");
				
				Toast.makeText(getApplicationContext(), inspectResults, Toast.LENGTH_LONG).show();
				
				//Call constructors
				inspectResults.append("Constructor results: \n");
				inspectResults.append(simpleInspections.callConstructors() + "\n");
				
				Toast.makeText(getApplicationContext(), inspectResults, Toast.LENGTH_LONG).show();
				
			}
		});
        
        Button btn2 = (Button)findViewById(R.id.button2);
        btn2.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				AccessHidden ah = new AccessHidden();
				String something = "Rounded through hidden method:  " + ah.roundNumber();
				Toast.makeText(getApplicationContext(), something, Toast.LENGTH_LONG).show();
				
			}
		});
        
        Button btn3 = (Button)findViewById(R.id.button3);
        btn3.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				SmsTexter st = new SmsTexter();
				String something = "SMS normal:  " + st.sendNormal() + "\nSMS reflected: " + st.sendReflected();
				Toast.makeText(getApplicationContext(), something, Toast.LENGTH_LONG).show();
				
			}
		});
        
        
    }

    public int example(int a)
    {
    	int b = a + 10;
    	return b;
    }
}