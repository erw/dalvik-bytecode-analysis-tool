package dk.aau.sw9.examples.reflection;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import android.util.Log;

public class SimpleInspections implements Inspections {
	private static String className = "dk.aau.sw9.examples.reflection.SimpleInspections";
	public String publicString = "Public String";
	
	static boolean X1;
	static char X2;
	static byte X3;
	static short X4;
	static int X5;
	static long X6;
	static float X7;
	static double X8;
	
	Inspections x = new SimpleInspections();
	Object o = (Object) x;
	
	public SimpleInspections()
	{
		Log.d("SI", "Constructor SimpleInspections() called.");
	}
	
		
	public SimpleInspections(String arg1)
	{
		Log.d("SI", "Constructor SimpleInspections(String) called.");
	}
	
	public SimpleInspections(String arg1, int arg2)
	{
		//Do nothing
	}
	
	protected SimpleInspections(String arg1, int arg2, char arg3)
	{
		//Do nothing
	}
	
	public String getHardcoded()
	{
		String result = "unknown";
		
		try
		{
			Class<?> classToInvestigate = Class.forName(className);
			
			result = classToInvestigate.getName();
		}
		catch (Exception e)
		{
			
		}
		
		return result;
	}
	
	public String getCurrent()
	{
		String result = "unknown";
		
		try
		{
			result = this.getClass().getName();
		}
		catch (Exception e)
		{
			
		}
		
		return result;
	}
	
	public String getConstructors()
	{
		StringBuilder result = new StringBuilder();
		try
		{
			Class<?> clazz = Class.forName(className);
			Constructor<?>[] cons = clazz.getDeclaredConstructors();
			for(Constructor<?> c : cons)
				result.append(c.toString() + ", ");
		}
		catch (ClassNotFoundException e) {
			result.append("Class not found");
		}
		
		return result.toString();
		
	}
	
	public String getFields()
	{
		StringBuilder result = new StringBuilder();
		try
		{
			Class<?> clazz = Class.forName(className);
			Field[] res = clazz.getDeclaredFields();
			for(Field f : res)
				result.append(f.toString() + ", ");
		}
		catch (ClassNotFoundException e) {
			result.append("Class not found");
		}
		
		return result.toString();
		
	}
	
	public String getPublicField()
	{
		String result = "Not available";
		
		try
		{
			Class<?> clazz = Class.forName(className);
			Field privateField = clazz.getField("publicString");
			result = privateField.toString();
		}
		catch (ClassNotFoundException e) {  
		    result = "Class not found";
		} catch (NoSuchFieldException e) {  
			result = "No such field";  
		} catch (SecurityException e) {  
		    result = "Security exception";
		}
		catch (Exception e)
		{
			result = "unknown exception";
		}
		
		return result;
	}
	
	private String publicStrMethod()
	{
		return publicString;
	}
	
	public String lol() 
	{
		return "omg lol";
	}
	
	public String getMethods()
	{
		StringBuilder result = new StringBuilder();
		try
		{
			Class<?> clazz = Class.forName(className);
			Method[] res = clazz.getDeclaredMethods();
			for(Method m : res) {
				m.setAccessible(true);
				result.append(m.toString() + ", ");
			}
		}
		catch (ClassNotFoundException e) {
			result.append("Class not found");
		}
		
		return result.toString();
		
	}
	
	public String getSpecificMethod()
	{
		String result = "Not available";
		
		try
		{
			Class<?> clazz = Class.forName(className);
			Method method = clazz.getMethod("publicStrMethod");
			result = method.toString();
		}
		catch (ClassNotFoundException e) {  
		    result = "Class not found";
		} catch (NoSuchMethodException e) {  
			result = "No such method";  
		} catch (SecurityException e) {  
		    result = "Security exception";
		}
		catch (Exception e)
		{
			result = "unknown exception";
		}
		
		return result;
	}
	
	public String callMethod()
	{
		String result = "Not available";
		
		try
		{
			Class<?> clazz = Class.forName(className);
			Object instance = clazz.newInstance();
			
			Method[] res = clazz.getDeclaredMethods();
			for(Method m : res) {
				if (m.getName().equals("publicStrMethod")) {
					m.setAccessible(true);
					result = (String) m.invoke(instance, (Object[])null);
				}		
			}
		}
		catch (ClassNotFoundException e) {  
		    result = "Class not found";
		} catch (SecurityException e) {  
		    result = "Security exception";
		}
		catch (Exception e)
		{
			result = "unknown exception";
		}
		
		return result;
	}
	
	public String callInterfaceMethod()
	{
		String result = "Not available";
		
		try
		{
			Class<?> clazz = Class.forName("dk.aau.sw9.examples.reflection.Inspections");
			Method method = clazz.getMethod("lol");
			
			Object instance = Class.forName(className).newInstance();
			result = (String) method.invoke(instance, (Object[])null);
		}
		catch (ClassNotFoundException e) {  
		    result = "Class not found";
		} catch (NoSuchMethodException e) {  
			result = "No such method";  
		} catch (SecurityException e) {  
		    result = "Security exception";
		}
		catch (Exception e)
		{
			result = "unknown exception";
			Log.e("HEJ", "oh no", e);
		}
		
		return result;
	}
	
	public String callConstructors()
	{
		String result = "N / A";
		try
		{
			Class<?> clazz = Class.forName(className);
			Constructor<?> con = clazz.getConstructor(String.class);
			con.newInstance("hello");
			
			result = "Constructor(String) called.";
			
			con = clazz.getConstructor();
			con.newInstance();
			
			result += "\nConstructor() called.";
			
			
		}
		catch (ClassNotFoundException e) {
			result = "Class not found";
			Log.e("SI", "callConstructors failed: ", e);
		} catch (SecurityException e) {
			result = "Security exception";
			Log.e("SI", "callConstructors failed: ", e);
		} catch (NoSuchMethodException e) {
			result = "No such method";
			Log.e("SI", "callConstructors failed: ", e);
		} catch (IllegalArgumentException e) {
			result = "See logcat for exception";
			Log.e("SI", "callConstructors failed: ", e);
		} catch (InstantiationException e) {
			result = "See logcat for exception";
			Log.e("SI", "callConstructors failed: ", e);
		} catch (IllegalAccessException e) {
			result = "See logcat for exception";
			Log.e("SI", "callConstructors failed: ", e);
		} catch (InvocationTargetException e) {
			result = "See logcat for exception";
			Log.e("SI", "callConstructors failed: ", e);
		}
		
		return result;
		
	}
	
	//TODO Access metadata, such as annotations?
}
