package dk.aau.sw9.examples.reflection;

import java.lang.reflect.Method;

import android.app.PendingIntent;
import android.telephony.SmsManager;

public class SmsTexter {

	public String sendNormal()
	{
		String result = "Message not sent.";
		
		SmsManager sm = SmsManager.getDefault();
		sm.sendTextMessage("24289513", null, "Text message", null, null);
		result = "Message sent?";
		
		return result;
		
	}
	
	public String sendReflected()
	{
		String result = "Message not sent.";
		
		try
		{
			Class<?> clazz = Class.forName("android.telephony.SmsManager");
			Method getDefault = clazz.getMethod("getDefault");
			Object manager = getDefault.invoke(null, (Object[])null);
			Method sendTextMessage = clazz.getMethod("sendTextMessage", new Class[] {String.class, String.class, String.class, PendingIntent.class, PendingIntent.class});
			sendTextMessage.invoke(manager, new Object[] {"24289513", null, "Text message reflected", null, null});
			
			result = "Message sent?";
		}
		catch (Exception e) {
			result = "Exception: " +e ;
		}
		
		
		return result;
		
	}
}
