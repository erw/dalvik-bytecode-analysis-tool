package dk.aau.sw9.examples.reflection;

import java.lang.reflect.Method;

public class AccessHidden {

	public static String TAG = "Reflection Examples, AccessHidden";
	
	public String roundNumber()
	{
		String result = "N/A";
		try {
			final Class<?> classCallManager = Class.forName("com.android.internal.util.FastMath");
			Method methodGetState = classCallManager.getDeclaredMethod("round", float.class);

			result = methodGetState.invoke(null, 3.2f).toString();

		} catch (Exception e) {
			result = e.toString();
			e.printStackTrace();
		}
		
		
		return result;
	}
	
}
