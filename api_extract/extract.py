#!/usr/bin/env python
# coding: UTF-8

import sys
import os
import fnmatch
import re
from lxml import etree

class Method(object):
        def __init__(self, identifier, clazz, method_name, arg_types, return_type):
                self.identifier = identifier
                self.clazz = clazz
                self.method_name = method_name
                self.arg_types = arg_types
                self.return_type = return_type

        def __str__(self):
                return str(vars(self))

class Clazz(object):
	def __init__(self, identifier, name, kind):
		self.identifier = identifier
		self.name = name
		self.kind = kind
		self.methods = [] # List of Method objects

	def __str__(self):
		return str(vars(self))

def convert_clazz_name(raw):
	tmp = re.sub('<.*>', '', raw)
	identifier = 'L' + tmp.replace('::', '/') + ';'
	name = tmp.split('::')[-1]
	return (identifier, name)

unknown_types = []
unknown_classes = []

def convert_prim_type(raw):
	global unknown_types
	array = raw.count('[]')
	if array:
		raw = raw.replace('[]', '')
	dic = {
		'int': 'I',
		'boolean': 'Z',
		'void': 'V',
		'byte': 'B',
		'short': 'S',
		'char': 'C',
		'long': 'J',
		'float': 'F',
		'double': 'D',
		'T' : 'Ljava/lang/Object;', # Generics, correct?
		'E' : 'Ljava/lang/Object;', # Generics
		'P' : 'Ljava/lang/Object;', # Generics
		'V' : 'Ljava/lang/Object;', # Generics
		'Result' : 'Ljava/lang/Object;', # Generics
		'Progress...' : '[Ljava/lang/Object;', # Generic varargs
		'String': 'Ljava/lang/String;',
		'Object': 'Ljava/lang/Object;',
		'Preference' : 'Landroid/preference/Preference;',
		'ISipSession': 'Landroid/net/sip/ISipSession;',
		'ContextMenu.ContextMenuInfo': 'Landroid/view/ContextMenu/ContextMenuInfo;',
		'InputMethodService.Insets' : 'Landroid/inputmethodservice/InputMethodService/Insets;',
		'AdapterView<?>': 'Landroid/widget/AdapterView;',
		'CharSequence': 'Ljava/lang/CharSequence;',
		'InternalInsetsInfo': 'Landroid/view/ViewTreeObserver/InternalInsetsInfo;',
		'Resources.Theme': 'Landroid/content/res/Resources/Theme;',
		'ConnectionParams' : 'Lcom/android/internal/telephony/DataConnection/ConnectionParams;',
		'WebStorage.QuotaUpdater' : 'Landroid/webkit/WebStorage/QuotaUpdater;',
		'GeolocationPermissions.Callback' : 'Landroid/webkit/GeolocationPermissions/Callback',
		'List< Cell >': 'Ljava/util/List;',
		'Throwable': 'Ljava/lang/Throwable;', 
		'WindowManager.LayoutParams': 'Landroid/view/WindowManager/LayoutParams;', 
		'UEventObserver.UEvent': 'Landroid/os/UEventObserver/UEvent;',
		'Key': 'Landroid/inputmethodservice/Keyboard/Key;', 
		'AdapterView': 'Landroid/widget/AdapterView;',
		'Insets': 'Landroid/inputmethodservice/InputMethodService/Insets;', 
		'Builder': 'Landroid/app/AlertDialog/Builder;', 
		'AlertDialog.Builder': 'Landroid/app/AlertDialog/Builder;', 
		'XmlPullParser': 'Lorg/xmlpull/v1/XmlPullParser;',
		'List< LockPatternView.Cell >': 'L/java/util/List;', 
		'Class': 'Ljava/lang/Class;', 
		'UEvent' : 'Landroid/os/UEventObserver/UEvent;', 
		'HashMap< String, Object >': 'Ljava/util/HashMap;', 
		'VolumeStore': 'Landroid/preference/VolumePreference/VolumeStore;',
		'IInputMethod': 'Lcom/android/internal/view/IInputMethod;',
		'IInputMethodSession' : 'Lcom/android/internal/view/IInputMethodSession;',
		'TerribleFailure': 'Landroid/util/Log/TerribleFailure;',

	}
	try:
		out = dic[raw]
		for i in range(0, array):
			out = '[' + out
		return out
	except KeyError:
		print >> sys.stderr, 'Unknown prim type: ' + raw
		unknown_types.append(raw)
		return 'UNKNOWN'

def parse_type(classes, elm):
	global unknown_classes
	ref = elm.find('ref')
	if ref is not None:
		try:
			return classes[ref.get('refid')].identifier
		except KeyError:
			print >> sys.stderr, 'UNKNOWN CLASS: ', ref.get('refid')
			unknown_classes.append(ref.get('refid'))
			return 'UNKNOWN'
	else:
		out = elm.text
		out = re.sub('^abstract ', '', out)
		out = re.sub('^final ', '', out)
		out = re.sub('^synchronized ', '', out)
		return convert_prim_type(out)

def main():
	global unknown_types
	global unknown_classes
	if len(sys.argv) == 1:
		print >> sys.stderr, "Usage: %s [doxygen_xml_path]" % sys.argv[0]
		sys.exit(1)

	doxygen_path = sys.argv[1]

	if not os.path.exists(doxygen_path):
		print >> sys.stderr, "Doxygen path not found"
		sys.exit(1)

	# Structures
	classes = {} # Dict of Clazz objects, key=doxygen (ref)id
	parents = [] # (parent, child) tuple list
	interfaces = [] # List of interfaces
	implements = [] # (interface, clazz) tuple list
	

	# Parse
	# 2-phases: Find all classes (for argument references), then all methods
	for phase in range(1,3):
		print "%%%%%% Phase %d" % phase
		for root, dirs, files in os.walk(doxygen_path):
			for filename in fnmatch.filter(files, '*.xml'):
				if not (filename.startswith('class') or filename.startswith('interface')):
					continue # Skip files without interfaces or classes
				file_path = os.path.join(root, filename)
#				print >> sys.stderr, 'Extracting from %s ...' % file_path
				with open(file_path) as f:
					if f.name.find('com_1_1android_1_1internal_1_1') > -1:
						continue # Skip internal class people can't use anyway

					tree = etree.parse(f)
					classes_raw = tree.xpath('//compounddef/compoundname')
					if len(classes_raw) != 1:
						print >> sys.stderr, "Error: %d classes found." % len(classes_raw)
						continue

					if phase == 1: #Phase 1, find classes
						clazz_identifier, clazz_name = convert_clazz_name(classes_raw[0].text)
						refid = classes_raw[0].getparent().get('id')
						kind = classes_raw[0].getparent().get('kind')
						clazz = Clazz(clazz_identifier, clazz_name, kind)
						classes.update({refid: clazz})
					else: # Phase 2, find methods
						clazz = classes.get(classes_raw[0].getparent().get('id'))
						methods = tree.xpath('//compounddef/sectiondef[@kind="protected-func" or @kind="public-func" or @kind="package-func"]/memberdef[@kind="function"]')
						for method in methods:
							method_name = method.find('name').text
							method_name = re.sub('<.*>', '', method_name)
							if not re.match('on[A-Z]', method_name):
								continue
#							if method_name == clazz.name: # Constructor, change name to <init>
#								method_name = '<init>'
							args_raw = method.findall('param/type')
							args = []
#							print 'Method: ', clazz.name, method_name, args_raw
							for arg in args_raw:
								args.append(parse_type(classes, arg))
							if method_name != '<init>':
								return_type = method.find('type')
								return_type = parse_type(classes, return_type)
							else:
								return_type = 'V'
							method_identifier = clazz.identifier + '->' + method_name + '(' + ''.join(args) + ')' + return_type
							m = Method(method_identifier, clazz.identifier, method_name, args, return_type)
							clazz.methods.append(m)
#							print 'Method: ', m
	
#	print '%%%UNKNOWN TYPES: ', unknown_types
#	print '%%%UNKNOWN CLASSES: ', unknown_classes
	
	classes_out = {}
	interfaces_out = {}
	
	for k, cl in classes.iteritems():
		if len(cl.methods)==0:
			continue
#		print 'kind: ', cl.kind
#		print 'Identifier: ', repr(cl.identifier)
#		print 'Methods: ', cl.methods
		methods = []
		for method in cl.methods:
			methods.append(method.identifier.split('->')[1])
		if cl.kind == 'interface':
			interfaces_out.update({cl.identifier: methods})
		else:
			classes_out.update({cl.identifier: methods})
	
	print '%%% Classes:'
	print classes_out
	
	print '%%% Interfaces:'
	print interfaces_out

	# Generate
#	output = ''
#
#	output += '\n%%% Classes\n'
#	clazz_lines = Set()
#	for clazz in Globals.classes:
#		clazz_lines.add("class('%s').\n" % clazz)
#	for parent, child in Globals.parents:
#		if parent != 'Ljava/lang/Object;':
#			clazz_lines.add("class('%s').\n" % parent)
#	for clazz_line in clazz_lines:
#		output += clazz_line
#	
#	output += '\n%%% Interfaces\n'
#	for interface in Globals.interfaces:
#		output += "interface('%s').\n" % interface
#	
#	output += '\n%%% Parent relationships\n'
#	for parent in Globals.parents:
#		output += "parent('%s', '%s').\n" % parent
#
#	output += '\n%%% Implements relationships\n'
#	for implements in Globals.implements:
#		output += "implements('%s', '%s').\n" % implements




if __name__ == '__main__':
	main()
	sys.stdout.flush()


