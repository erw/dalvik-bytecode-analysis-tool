#!/bin/bash
time for app in $(find -name smali | sed -e h -e 's:^..::' -e 's:/smali$::' -e 's:\.:/:g' -e H -e x -e 's:\n:/:'); do
	(
		cd $app;
		echo $app;
		find -name 'a.smali' | wc -l;
		find -type f | grep -vf ~/9c/apk_analysis/lib_patterns | wc -l;
		find -type f | grep -vf ~/9c/apk_analysis/lib_patterns | grep '\$' | wc -l;
		find -type f | grep -vf ~/9c/apk_analysis/lib_patterns | grep '\$[0-9]' | wc -l;
	)
done \
	| sed 'N;N;N;N;s/\n/, /g' \
	| sed \
		-e "s/^/'/" \
		-e "s/, /', /" \
		-e 's/.*/[&],/'
