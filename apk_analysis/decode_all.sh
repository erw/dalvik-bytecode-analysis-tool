#!/bin/bash

n=0
for apk in $(find . -name '*.apk' -a ! -name 'pkg.apk'); do
	((n += 1))
	echo
	date
	echo -e "$n\tDecoding $apk"
	! apktool decode -f $apk ${apk/-1*/} && echo "$apk" >> decoding_log
done
