#!/usr/bin/env python
# -*- coding: UTF-8 -*-

from __future__ import division
import sys
import os
import re
import fnmatch
from operator import itemgetter

# To be run from a category or above since it needs the Java package name with dots as part of the filesystem path


def file_contains(file_path, needle):
	with open(file_path) as f:
		for line in f.readlines():
			if needle in line:
				return True
	return False


def main():
	root_path = sys.argv[1] if len(sys.argv) > 1 else '.'

	apps_classes = []
	apps_features = []

	for root, dirs, files in os.walk(root_path):
		for dirname in fnmatch.filter(dirs, 'smali'):
			pathname = re.sub('.*/', '', root)
			dirs = pathname.split('.')
			dirs.insert(0, '')
			curdir = os.path.join(root, dirname)

			num_classes = [0, 0, 0]  # total, inner, inner anon
			uses_features = [False, False, False, False]

			def inc_classes(f):
				num_classes[0] += 1
				if '$' in f:
					num_classes[1] += 1
					if re.match('.*\$[0-9]', f):
						num_classes[2] += 1

			features = [
					'java/lang/Thread',
					'java/lang/reflect',
					'java/lang/ClassLoader',
					'java/lang/Runtime;->exec',
			]

			def inc_features(f_path):
				for index, feature in enumerate(features):
					if not uses_features[index] and file_contains(f_path, feature):
						uses_features[index] = True

			# descend into the path named by the package name
			for d in dirs:
				curdir = os.path.join(curdir, d)
				if os.path.isdir(curdir):
					for f in [x for x in os.listdir(curdir) if x.endswith('.smali')]:
						inc_classes(f)
						inc_features(os.path.join(curdir, f))
				else:
					break

			# walk inner package name dir
			for root, dirs, files in os.walk(curdir):
				for f in [x for x in files if x.endswith('.smali')]:
					inc_classes(f)
					# if curdir != root:
					# 	print curdir, root
					inc_features(os.path.join(root, f))

			apps_classes.append(num_classes)
			apps_features.append(uses_features)
			# print pathname, num_classes
	

	if not apps_classes:
		sys.exit(1)

	la = len(apps_classes)

	# print '\nAverage'
	# print sum([app[0] for app in apps_classes]) / la
	# print sum([app[1] for app in apps_classes]) / la
	# print sum([app[2] for app in apps_classes]) / la

	# print '\nMedian'
	# print [app[0] for app in sorted(apps_classes, key=itemgetter(0))][la//2]
	# print [app[1] for app in sorted(apps_classes, key=itemgetter(1))][la//2]
	# print [app[2] for app in sorted(apps_classes, key=itemgetter(2))][la//2]

	# print '\nMax'
	# print max([app[0] for app in apps_classes])
	# print max([app[1] for app in apps_classes])
	# print max([app[2] for app in apps_classes])

	# print '\nMin'
	# print min([app[0] for app in apps_classes])
	# print min([app[1] for app in apps_classes])
	# print min([app[2] for app in apps_classes])

	#
	print '\nFeature use'
	print '%.2f' % (sum([app[0] for app in apps_features]) / la * 100)
	print '%.2f' % (sum([app[1] for app in apps_features]) / la * 100)
	print '%.2f' % (sum([app[2] for app in apps_features]) / la * 100)
	print '%.2f' % (sum([app[3] for app in apps_features]) / la * 100)



if __name__ == '__main__':
	main()
