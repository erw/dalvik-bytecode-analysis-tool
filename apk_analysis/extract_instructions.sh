#!/bin/bash
find . -name '*.smali' \
	| xargs cat \
	| pv -c \
	| sed 's/^\s*//' \
	| grep -v \
		-e '^$' \
		-e '^\.line' \
		-e '^#' \
		-e '^}' \
		-e '^:' \
		-e '^0x' \
		-e '^-0x' \
		-e '^"' \
	| sed \
		-e '/^\.annotation/,/^\.end annotation/d' \
	| grep -v \
		-e '^\.' \
	| sed \
		-e 's/ .*//' \
		-e 's/^add-.*lit.*/binop-lit/' \
		-e 's/^and-.*lit.*/binop-lit/' \
		-e 's/^div-.*lit.*/binop-lit/' \
		-e 's/^mul-.*lit.*/binop-lit/' \
		-e 's/^or-.*lit.*/binop-lit/' \
		-e 's/^rem-.*lit.*/binop-lit/' \
		-e 's/^shl-.*lit.*/binop-lit/' \
		-e 's/^shr-.*lit.*/binop-lit/' \
		-e 's/^ushr-.*lit.*/binop-lit/' \
		-e 's/^sub-.*lit.*/binop-lit/' \
		-e 's/^xor-.*lit.*/binop-lit/' \
		-e 's/^rsub-.*lit.*/binop-lit/' \
		-e 's/\/.*//' \
		-e 's/^if-.*z$/ifz/' \
		-e 's/^if-.*/if/' \
		-e 's/^aget-.*/aget/' \
		-e 's/^aput-.*/aput/' \
		-e 's/^iget-.*/iget/' \
		-e 's/^iput-.*/iput/' \
		-e 's/^sget-.*/sget/' \
		-e 's/^sput-.*/sput/' \
		-e 's/^add-.*/binop/' \
		-e 's/^and-.*/binop/' \
		-e 's/^div-.*/binop/' \
		-e 's/^mul-.*/binop/' \
		-e 's/^or-.*/binop/' \
		-e 's/^rem-.*/binop/' \
		-e 's/^shl-.*/binop/' \
		-e 's/^shr-.*/binop/' \
		-e 's/^ushr-.*/binop/' \
		-e 's/^sub-.*/binop/' \
		-e 's/^xor-.*/binop/' \
		-e 's/^rsub-.*/binop/' \
		-e 's/^not-.*/unop/' \
		-e 's/^neg-.*/unop/' \
		-e 's/.*-to-.*/unop/' \
		-e 's/^cmp.*/cmp/' \
		-e 's/^const-string.*/const-string/' \
		-e 's/^const-class.*/const-class/' \
		-e 's/^const-wide.*/const/' \
		-e 's/^move-wide.*/move/' \
		-e 's/^move-result.*/move-result/' \
		-e 's/^move-object.*/move/' \
		-e 's/^move\/.*/move/' \
		-e 's/^return-object.*/return/' \
		-e 's/^return-wide.*/return/' \
	| pv -c \
	| sort \
	| uniq -c \
	| sort -nr
