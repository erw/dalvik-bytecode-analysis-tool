#!/usr/bin/env python
# coding: UTF-8

'''
For all the AndroidManifest.xml files given as arguments, 
'''

from __future__ import division
import sys
from lxml import etree
from operator import itemgetter

NAMESPACES = {'android': 'http://schemas.android.com/apk/res/android'}

# num_manifests = len(sys.argv) - 1  # Ignore this program
num_manifests = 0
len_longest_filename = 0

app_perms_map = {}
perm_count_map = {}

for filename in sys.argv[1:]:

	len_longest_filename = max(len_longest_filename, len(filename))

	root = etree.parse(filename)

	perm_names = root.xpath('/manifest/uses-permission/@android:name', namespaces=NAMESPACES)
	perm_names = set(perm_names)  # Retarded developers

	app_perms_map[filename] = perm_names

	if 'android.permission.AUTHENTICATE_ACCOUNTS' in perm_names:
		num_manifests += 1
		for perm_name in perm_names:
			perm_count_map.setdefault(perm_name, 0)
			perm_count_map[perm_name] += 1

perm_count_pairs = list(perm_count_map.iteritems())
perm_count_pairs.sort(key=itemgetter(1), reverse=True)

app_perms_pairs = list(app_perms_map.iteritems())
app_perms_pairs.sort(key=lambda x: len(x[1]))


# Number of permissions by manifest, sorted by number of permissions
# for filename, perm_names in app_perms_pairs:
# 	print len(perm_names), '\t', filename

# print '-' * 80
# 
# # Print all permissions for all manifests, sorted by filename
# app_perms_pairs.sort()
# for filename, perm_names in app_perms_pairs:
# 	category, package_name, _ = filename.split('__')
# 	for perm_name in perm_names:
# 		# print category, package_name,
# 		print filename,
# 		print perm_name

print '-' * 80

for perm, count in perm_count_pairs:
	print '%d\t%.1f%%\t%s' % (count, count/num_manifests*100, perm)
