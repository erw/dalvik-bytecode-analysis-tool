#!/usr/bin/env python
# coding: UTF-8

from __future__ import division
import sys
from lxml import etree
from operator import itemgetter

NAMESPACES = {'android': 'http://schemas.android.com/apk/res/android'}

num_manifests = len(sys.argv) - 1  # Ignore this program

rows = []

for filename in sys.argv[1:]:

	root = etree.parse(filename)

	elems_activity = root.xpath('/manifest/application/activity/@android:name', namespaces=NAMESPACES)
	elems_service = root.xpath('/manifest/application/service/@android:name', namespaces=NAMESPACES)
	elems_provider = root.xpath('/manifest/application/provider/@android:name', namespaces=NAMESPACES)
	elems_receiver = root.xpath('/manifest/application/receiver/@android:name', namespaces=NAMESPACES)
	# print filename, len(elems_activity), len(elems_service), len(elems_provider), len(elems_receiver)
	rows.append([filename, len(elems_activity), len(elems_service), len(elems_provider), len(elems_receiver)])

### Percentage
# print len([row for row in rows if row[1] > 0]) / len(rows) * 100
# print len([row for row in rows if row[2] > 0]) / len(rows) * 100
# print len([row for row in rows if row[3] > 0]) / len(rows) * 100
# print len([row for row in rows if row[4] > 0]) / len(rows) * 100

### Avg num of
# print sum([row[1] for row in rows]) / len(rows)
# print sum([row[2] for row in rows]) / len(rows)
# print sum([row[3] for row in rows]) / len(rows)
# print sum([row[4] for row in rows]) / len(rows)

### Max num of
# print max([row[1] for row in rows])
# print max([row[2] for row in rows])
# print max([row[3] for row in rows])
# print max([row[4] for row in rows])

### Histogram
# for n in xrange(123+1): print n, ';', len([row for row in rows if row[1] == n])
# for n in xrange(23+1): print n, ';', len([row for row in rows if row[2] == n])
# for n in xrange(12+1): print n, ';', len([row for row in rows if row[3] == n])
# for n in xrange(43+1): print n, ';', len([row for row in rows if row[4] == n])

# print 'Median'
# print [row[1] for row in sorted(rows, key=itemgetter(1))][1700//2]
# print [row[2] for row in sorted(rows, key=itemgetter(2))][1700//2]
# print [row[3] for row in sorted(rows, key=itemgetter(3))][1700//2]
# print [row[4] for row in sorted(rows, key=itemgetter(4))][1700//2]
