#!/bin/bash

echo
echo "==== Getting from /data/app"

for f in $(adb shell ls /data/app | dos2unix | grep -v '\.zip$' | grep -v 'org\.connectbot' | grep -v 'com\.android\.vending' | grep -v 'com\.google\.earth' | grep -v 'com\.google\.android\.voicesearch'); do
	echo
	echo $f
	adb pull /data/app/${f}
	adb uninstall ${f/-1.*/}
done

echo
echo "==== Getting from /data/app-private"

for f in $(adb shell ls /data/app-private | dos2unix); do
	echo
	echo $f
	adb pull /data/app-private/${f}
	adb uninstall ${f/-1.*/}
done

echo
echo "==== Getting from /mnt/asec"

for f in $(adb shell df | dos2unix | grep '^/mnt/asec/' | sed -e 's:/mnt/asec/::' -e 's/\s.*//'); do
	echo
	echo $f
	mkdir -p from_asec/$f
	adb pull /mnt/asec/${f} from_asec/$f
	adb uninstall ${f/-1/}
done
