#!/bin/bash

# Create symlinks so all apks from asec appear as normal apks that can then be decoded

find . -type f -name pkg.apk | sed 's/^\(.*\)\(\/from_asec\)\(.*\)\(\/pkg.apk\)$/(cd \1; ln -vs ".\2\3\4" ".\3.apk")/' | bash

# for f in $(find . -type f -name '*.zip'); do
# 	mv -vi $f ${f}.apk;
# done
