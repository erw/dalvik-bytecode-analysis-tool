/**
* Script has to be invoked from a valid browser market session,
* e.g. using the JavaScript Console in Google Chrome
*/

function install(pkg)
{
	alert('Installing: ' + pkg);
	var http = new XMLHttpRequest();
	var url = "/install";
	var params = "id=" + pkg + "&device=g79aeae3612ca4c4f&token=" + initProps['token'] + "&xhr=1";
	http.open("POST", url, true);

	http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

	//TODO These two lines does not seem to work
//	http.setRequestHeader("Content-length", params.length);
//	http.setRequestHeader("Connection", "close");

	http.onreadystatechange = function() {//Call a function when the state changes.
		if(http.readyState == 4 && http.status == 200) {
			alert(pkg + ": " + http.responseText);
		}
	}
	http.send(params);
}

function installAllApps()
{
	var elems = document.getElementsByTagName("a");
	var num = 0;
	for(i = 0; i < elems.length; i++)
	{
		elem = elems[i];
		if(elem.attributes.getNamedItem("data-docId")!=null)
		{
			val = elem.attributes.getNamedItem("data-docId").value;
			setTimeout("install('"+val+"')", num*1000);
			num++;
		}
	}
}

