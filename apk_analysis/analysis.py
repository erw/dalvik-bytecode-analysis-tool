#!/usr/bin/env python
# -*- coding: UTF-8 -*-

from __future__ import division
import sys
import os
import fnmatch
from sets import Set

class App(object):
    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)

def main():
	root_path = sys.argv[1] if len(sys.argv) > 1 else '.'

	apps = []

	index = 0
	print '   #', 'App'.ljust(75),
	print 'Obfusc'.ljust(10),
	print '.so libs'.ljust(10),
	print 'Thread'.ljust(10), 'Thread#'.ljust(10),
	print 'sleep'.ljust(10), 'sleep#'.ljust(10),
	print 'Reflect'.ljust(10), 'Reflect#'.ljust(10),
	print 'classload'.ljust(10), 'classload#'.ljust(10),
	print 'exec'.ljust(10), 'exec#'.ljust(10),
	print

	for root, dirs, files in os.walk(root_path):
		for filename in fnmatch.filter(dirs, 'smali'):
			app_path = os.path.join(root, filename)
			index += 1
			print '%4d' % index ,
			print app_path[2:-6].ljust(75) ,

			# obf
			is_obf = is_obfuscated(app_path)
			print ('OBF' if is_obf else '-').ljust(10) ,

			# libs
			num_libs = count_lib(app_path)
			print (str(num_libs) if num_libs > 0 else '-').ljust(10) ,

			# Thread
			num_thread, num_thread_nolibs = count_string(app_path, 'java/lang/Thread')
			print (str(num_thread) if num_thread > 0 else '-').ljust(10) ,
			print (str(num_thread) if num_thread_nolibs > 0 else '-').ljust(10) ,

			# sleep
			num_sleep, num_sleep_nolibs = count_string(app_path, 'java/lang/Thread;->sleep')
			print (str(num_sleep) if num_sleep > 0 else '-').ljust(10) ,
			print (str(num_sleep) if num_sleep_nolibs > 0 else '-').ljust(10) ,

			# Reflection
			num_reflection, num_reflection_nolibs = count_string(app_path, 'java/lang/reflect')
			print (str(num_reflection) if num_reflection > 0 else '-').ljust(10) ,
			print (str(num_reflection_nolibs) if num_reflection_nolibs > 0 else '-').ljust(10) ,

			# ClassLoader
			num_classloader, num_classloader_nolibs = count_string(app_path, 'java/lang/ClassLoader')
			print (str(num_classloader) if num_classloader > 0 else '-').ljust(10) ,
			print (str(num_classloader_nolibs) if num_classloader_nolibs > 0 else '-').ljust(10) ,

			# exec
			num_exec, num_exec_nolibs = count_string(app_path, 'java/lang/Runtime;->exec')
			print (str(num_exec) if num_exec > 0 else '-').ljust(10) ,
			print (str(num_exec_nolibs) if num_exec_nolibs > 0 else '-').ljust(10) ,

			print

			apps.append(App(
				is_obf=is_obf,
				num_libs=num_libs,
				num_thread=num_thread,
				num_thread_nolibs=num_thread_nolibs,
				num_sleep=num_sleep,
				num_sleep_nolibs=num_sleep_nolibs,
				num_reflection=num_reflection,
				num_reflection_nolibs=num_reflection_nolibs,
				num_classloader=num_classloader,
				num_classloader_nolibs=num_classloader_nolibs,
				num_exec=num_exec,
				num_exec_nolibs=num_exec_nolibs,
				))

	# num_apps = len(apps)

	# from IPython.Shell import IPShellEmbed
	# ipshell = IPShellEmbed()
	# ipshell()

	l100 = len(apps) / 100

	p_thread = len([app.num_thread for app in apps if app.num_thread > 0]) / l100
	p_thread2 = ( ( len([app.num_thread_nolibs for app in apps if app.num_thread_nolibs > 0]) / l100 ) * 100 / p_thread )
	print '%.2f, %.2f' % (p_thread, p_thread2)

	p_sleep = len([app.num_sleep for app in apps if app.num_sleep > 0]) / l100
	p_sleep2 = ( ( len([app.num_sleep_nolibs for app in apps if app.num_sleep_nolibs > 0]) / l100 ) * 100 / p_sleep )
	print '%.2f, %.2f' % (p_sleep, p_sleep2)

	p_reflection = len([app.num_reflection for app in apps if app.num_reflection > 0]) / l100
	p_reflection2= ( ( len([app.num_reflection_nolibs for app in apps if app.num_reflection_nolibs > 0]) / l100 ) * 100 / p_reflection )
	print '%.2f, %.2f' % (p_reflection, p_reflection2)

	p_classloader = len([app.num_classloader for app in apps if app.num_classloader > 0]) / l100
	p_classloader2=( ( len([app.num_classloader_nolibs for app in apps if app.num_classloader_nolibs > 0]) / l100 ) * 100 / p_classloader )
	print '%.2f, %.2f' % (p_classloader, p_classloader2)

	p_exec = len([app.num_exec for app in apps if app.num_exec > 0]) / l100
	p_exec2= ( ( len([app.num_exec_nolibs for app in apps if app.num_exec_nolibs > 0]) / l100 ) * 100 / p_exec )
	print '%.2f, %.2f' % (p_exec, p_exec2)






def is_obfuscated(app_path):
	# Approximation: False positive on a real class "a" and false negative on obfuscation not using "a.java"
	for root, dirs, files in os.walk(app_path):
		for filename in fnmatch.filter(files, '*.smali'):
			smali_file_path = os.path.join(root, filename)
			if filename == 'a.smali':
				return True
	return False


def has_lib(app_path):
	# Approximation: False positive on empty lib dir
	for root, dirs, files in os.walk(os.path.join(app_path, '..')):
		if fnmatch.filter(dirs, 'lib'):
			return True
	return False


def count_lib(app_path):
	# Approximation: False positive on empty lib dir
	sos = Set()
	for root, dirs, files in os.walk(os.path.join(app_path, '..')):
		if fnmatch.filter(dirs, 'lib'):
			for root, dirs, files in os.walk(os.path.join(app_path, '../lib')):
				for filename in fnmatch.filter(files, '*.so'):
					sos.add(filename)
	return len(sos)


lib_patterns = [
    'com/google/common',
    'com/google/gdata',
    'com/google/protobuf',
    'com/google/zxing',
    'com/google/android/imageloader',
    'com/google/api/client',
    'com/google/devtools',
    'com/google/tts',
	'android/support/v',
	'com/admob/android/ads',
	'com/adwhirl/adapters',
	'com/google/ads',
	'com/google/gson',
	'com/google/inject',
	'com/greystripe/android/sdk',
	'com/j256/ormlite',
	'com/mobclick/android',
	'com/mobclix/android/sdk',
	'com/phonegap',
	'com/pontiflex/mobile',
	'net/youmi/android',
	'com/millennialmedia/android',
	'com/inmobi/androidsdk',
	'oauth',
	'org/acra',
	'org/apache/commons',
	'org/apache/felix',
	'org/apache/harmony.beans',
	'org/apache/harmony.javax',
	'org/apache/http',
	'org/apache/lucene',
	'org/apache/poi',
	'org/apache/thrift',
	'org/aspectj',
	'org/codehaus/jackson',
	'org/json',
	'org/springframework',
	'roboguice',
	'twitter4j',
    ]

def count_string(app_path, needle):
	# Approximation: False positive on needle contained in constant string, method name, etc.
	out_all = 0
	out_nolibs = 0
	for root, dirs, files in os.walk(app_path):
		in_lib = any(filter(lambda x: x != -1, map(root.find, lib_patterns)))
		for filename in fnmatch.filter(files, '*.smali'):
			smali_file_path = os.path.join(root, filename)
			num = file_contains_num(smali_file_path, needle)
			out_all += num
			if not in_lib:
				out_nolibs += num
	return (out_all, out_nolibs)


def file_contains(file_path, needle):
	with open(file_path) as f:
		for line in f.readlines():
			if needle in line:
				return True
	return False


def file_contains_num(file_path, needle):
	out = 0
	with open(file_path) as f:
		for line in f.readlines():
			out += line.count(needle)
	return out


if __name__ == '__main__':
	main()
