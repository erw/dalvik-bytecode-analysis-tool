#!/bin/bash
find . -name '*.smali' \
	| xargs cat \
	| pv -c \
	| sed 's/^\s*//' \
	| sed \
		-e '/^\.annotation/,/^\.end annotation/d' \
	| grep '^invoke' \
	| sed 's/ {[^}]*}, / /' \
	| sed 's/[a-z-]* //' \
	| pv -c \
	| sort \
	| uniq -c \
	| sort -nr
