#!/bin/bash
echo
adb shell ls /data/app | dos2unix | grep -v '\.zip$' | grep -v -f ${0}_exclude_patterns | tee >(wc -l)
echo
adb shell ls /data/app-private | dos2unix | tee >(wc -l)
echo
adb shell df | grep ^/mnt/asec/ | tee >(wc -l)
