package dk.aau.sw9.examples;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import android.util.Log;

public class MyClass {
	public int value;
	public int foo(int a)
	{
		return 400;
	}
	
	public static String staticFoo(int j, String s)
	{
		if (j > 10) {
			return "dk.aau.sw9.examples.MySubClass";
		}
		else if (j > 20) {
			return s + ".blah";
		}
		return s;
	}

	protected void bar() {
		
		Class<?> c2 = double.class;
		Object instance2;
		try {
			instance2 = c2.newInstance();
			Method m2 = c2.getMethod("bar", new Class[] {int.class});
			int i = (Integer)m2.invoke(instance2, new Object[] {27});
			Log.d("TAG4", ""+i);
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	

	public static void staticBar(Object o) {
		
		Class<?> c2 = o.getClass();
		Object instance2;
		try {
			instance2 = c2.newInstance();
			Method m2 = c2.getMethod("staticBar", new Class[] {int.class});
			int i = (Integer)m2.invoke(instance2, new Object[] {27});
			Log.d("TAG4", ""+i);
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
