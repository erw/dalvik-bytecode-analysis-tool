package dk.aau.sw9.examples;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;

public class MiniAppActivity extends Activity {
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);
    	
    	
    	ArrayList<String> al = new ArrayList<String>();
    	al.add("My string");
    	al.add("String2");
    	
    	String s = al.get(0);
    	ArrayList<String> al2 = numbers();
    	s = al2.get(1);
    	
    }
	
	public ArrayList<String> numbers()
	{
		ArrayList<String> r = new ArrayList<String>();

		Context context = this.getApplicationContext();

		Cursor cursor = context.getContentResolver().query(
				ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null,
				null, null);

		int phoneNumberIndex = cursor
				.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.NUMBER);

		try {
			while (cursor != null && cursor.moveToNext()) {
				r.add(cursor.getString(phoneNumberIndex));
			}
		} finally {
			cursor.close();
		}
		
		return r;
	}
	

}
