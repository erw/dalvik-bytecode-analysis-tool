package my.pkg;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import my.pkg.R;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

public class AnalysisTestActivity extends Activity {
	
	int j;
	static final int staticint = 123;
	final int nonstaticint = 123;
	
	// default constructor cannot resolve Activity.<init>

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);  // unresolved
        setContentView(R.layout.main);       // unresolved
        
        ISomeInterface x = new ClassC(100);  // init resolved
        
        // unresolved: Log.d, sb.<init>, sb.append, sb.toString
        Log.d("TAG", "" + x.increase(25));   // resolved to ClassC.increase
        
        ClassC.print();                      // resolves to ClassB.print
        
//        privateMethod(
        		((ClassB) x).increase(50)    // resolved to ClassC.increase
//        		);                           // resolved
        ;
        		
        int y = ClassB.factorial(5);
        
        ClassB b = new ClassB(22);
        
        synchronized (b) {
        	Toast.makeText(this, "5! = " + y, Toast.LENGTH_SHORT).show();
            
//            b.pleaseCallMe(33);	
		}
        
        
        reflectionTest();
        
        try {
			test();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

    }
    
    public void reflectionTest()
    {
    	Class<?> clazz;
    	Method method;
 		try {
 			clazz = Class.forName("my.pkg.ClassB");
 	        method = clazz.getMethod("factorial", int.class);
 	        method = clazz.getMethod("increase", int.class);
 	        Integer result = (Integer) method.invoke(clazz.newInstance(), 3);
 			
 			clazz = Class.forName("my.pkg.AnalysisTestActivity");
 	        method = clazz.getMethod("reflectionTest");
 	        method.invoke(clazz.newInstance());
 		} catch (Exception e) {
 			e.printStackTrace();
 		}
    }
    
    public void test() throws Exception
    {
    	try
    	{
    		AnalysisTestActivity.class.getMethod("innerTest").invoke(this);
    	}
    	catch(NegativeArraySizeException e)
    	{
    		//
    	}
    }
    
    public void innerTest()
    {
    	int[] a = new int[32];
    	a[0] = 12;
    }
    
}