package my.pkg;

public interface ISomeInterface {

	int ifaceConstant1 = 1001;
	int ifaceConstant2 = Integer.parseInt("1002");  // Creates <clinit>
	// unresolved: Integer.parseInt
	
	int increase(int i);
}
