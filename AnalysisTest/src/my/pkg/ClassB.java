package my.pkg;

import android.util.Log;

public class ClassB extends ClassA implements ISomeInterface {

	public ClassB(int i) {
		super(i - 1);  // resolved to ClassA.<init>
	}

	@Override
	public int increase(int i) {
		return i + 1;
	}
	
	public static void print() {
        // unresolved: Log.d, sb.<init>, sb.append, sb.toString
		Log.d("TAG", "" + staticint);
	}
	
	public static int factorial(int x) {
		if (x <= 1) {
			return x;
		}
		return x * factorial(x - 1);  // resolved
	}

//	public int pleaseCallMe(int x) {
//		if (x > 100) {
//			return pleaseCallMe(44);  // resolved
//		}
//		return 23;
//	}
}
