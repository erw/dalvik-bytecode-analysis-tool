package my.pkg;

import android.util.Log;

public class ClassC extends ClassB {

	public ClassC(int i) {
		super(i - 1);  // resolved to ClassB.<init>
	}

	@Override
	public int increase(int i) {
		Log.d("TAG", "" + staticint);
		return i + 5;
	}

}
