package my.pkg;

public abstract class ClassA {

	public static int staticint;
	
	public ClassA(int i)
	{
		// automatic call to Object.<init> resolved
		staticint = i;
	}
	
}
