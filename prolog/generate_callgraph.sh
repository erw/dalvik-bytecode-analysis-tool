#!/bin/bash
prolog_script_name="out.pl"
basename="callgraph"
basenamedot="${basename}.dot"
basenamedottmp="${basenamedot}.tmp"
basenamepng="${basename}.png"

run_dot=0
do_stats=0
if [ $# -gt 0 ]; then
	if [ $1 = "--dot" ]; then
		run_dot=1
		shift
	elif [ $1 = "--stats" ]; then
		do_stats=1
		shift
	fi
fi

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

time $DIR/generator.py . --nowarn > ${prolog_script_name}

if [ $do_stats -eq 1 ]; then
	time xsb -S --noprompt -e "['${prolog_script_name}'], printMethodCalls, printStats, halt." \
		> ${basenamedot}
else

	echo 'digraph name {' > ${basenamedot}
	echo 'node [label="\N", fontname=Monospace, fontsize=8, shape=plaintext];' >> ${basenamedot}
	echo 'edge [fontsize=10];' >> ${basenamedot}
	echo 'subgraph cluster_api_methods { graph [label=API, style=rounded];' >> ${basenamedot}

	time xsb -S --noprompt -e "['${prolog_script_name}'], printMethodCalls, halt." \
		| sort \
		| uniq \
		| sed 's/: /:\\n/g' \
		| sed 's/;->/&\\n/g' \
		| sed 's/\//\\n&/g' \
		> ${basenamedottmp}

	cat ${basenamedottmp} \
		| sed '/"API:/!d; s/.* -> //' \
		| sort \
		| uniq \
		>> ${basenamedot}

	# end subgraph
	echo '}' >> ${basenamedot}

	# append edges
	cat ${basenamedottmp} \
		>> ${basenamedot}

	# end digraph
	echo '}' >> ${basenamedot}

	# remove API text
	cat ${basenamedot} \
		| sed 's/API:\\n//g' \
		> ${basenamedottmp}

	mv ${basenamedottmp} ${basenamedot}
fi


if [ $run_dot -eq 1 ]; then
	dot -Tpng -o ${basenamepng} ${basenamedot}
	gnome-open ${basenamepng}
fi
