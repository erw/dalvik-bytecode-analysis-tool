#!/usr/bin/env python
# coding: UTF-8


import sys
from generator import *
from cStringIO import StringIO

### Generator helpers
def quote(string):
	return "'%s'" % string

def warning_unresolved_method(mid, kind=''):
	if not Globals.generate_warnings:
		return ''
	return "writeln('\033[01;31mWARNING\033[00m, unresolved %smethod: %s')" % (('%s ' % kind) if kind else '', mid)

UNRESOLVED_MID = 'UNRESOLVED method call'

def is_reference_type(arg_type):
	return arg_type.startswith('L') or arg_type.startswith('[')

def is_possibly_mutable_reference_type(arg_type):
	return is_reference_type(arg_type) and arg_type != 'Ljava/lang/String;'

def hatRraw(method_identifier, pc, reg, content):
	if not method_identifier in Globals.hatR_map:
		Globals.hatR_map[method_identifier] = len(Globals.hatR_map)
	if HATR_SPLIT:
		return "hatR%d(%s, %s, %s)" % (Globals.hatR_map[method_identifier], pc, reg, content)
	else:
		return "hatR(%s, %s, %s, %s)" % (method_identifier, pc, reg, content)

def hatR(address, reg, content):
	return hatRraw(quote(address.method.identifier), address.pc, reg, content)

def hatHraw(cl, addrMID, addrPC, field, value):
	return "hatH((%s, %s, %s), %s, %s)" % (cl, addrMID, addrPC, field, value)

def hatH(cl, addrMID, addrPC, field, value):
	return hatHraw(cl, addrMID, addrPC, quote(field), value)

def hatHnew(cl, address, field, value):
	return hatHraw(cl, quote(address.method.identifier), address.pc, quote(field), value)

def methodCall(caller, callee, callType='regular'):
	return "methodCall(%s, %s, %s)" % (caller, callee, callType)

def clause(*items):
	items = [item for item in items if item]  # convert tuple to list and filter out empty conditions
	out = StringIO()
	out.write(items.pop(0))
	if out.getvalue().startswith('%'):
		out.write('\n' + items.pop(0))
	if items:
		out.write(' :-\n\t')
		out.write(',\n\t'.join(items))
	out.write('.\n')
	out_ = out.getvalue()
	out.close()  # dunno why this helps reduce memory consumption but it does
	return out_

def handle(address, exception_clazz):

	if not RUNTIME_EXCEPTIONS:
		return ''

	if EXPLODE_TRANSFERS:
		reg_numbers = range(address.method.num_regs_total)
		clauses_transfer_other = (
			"% If handler found, transfer other registers\n"
			+ ''.join(clause(
				hatRraw(quote(address.method.identifier), 'PCH', reg_number, 'Y'),
				"findHandler(%s, %d, %s, PCH)" % (quote(address.method.identifier), address.pc, quote(exception_clazz)),
				"PCH \= bot",
				hatR(address, reg_number, 'Y'),
				) for reg_number in reg_numbers)
			)
	else:
		clauses_transfer_other = clause(
				hatRraw(quote(address.method.identifier), 'PCH', 'V', 'Y'),
				"findHandler(%s, %d, %s, PCH)" % (quote(address.method.identifier), address.pc, quote(exception_clazz)),
				"PCH \= bot",
				hatR(address, 'V', 'Y'),
			)
	return ''.join([
		clause("% If handler found, transfer exception reference to retval",
			hatRraw(quote(address.method.identifier), "PCH", "retval", quote(exception_clazz)),
			"findHandler(%s, %d, %s, %s)" % (quote(address.method.identifier), address.pc, quote(exception_clazz), "PCH"),
			"PCH \= bot",
			),
		clauses_transfer_other,
		clause("% If no handler found, add exception to cache",
			"hatE(%s, %s)" % (quote(address.method.identifier), quote(exception_clazz)),
			"findHandler(%s, %d, %s, PCH)" % (quote(address.method.identifier), address.pc, quote(exception_clazz)),
			"PCH = bot",
			),
	])



def transfer_exclude(address, reg_exclude):
	if EXPLODE_TRANSFERS:
		reg_numbers = range(address.method.num_regs_total)
		if reg_exclude != 'retval':
			reg_numbers.remove(reg_exclude)
		return ''.join(clause(hatR(address.next(), reg_number, "Y"), hatR(address, reg_number, "Y")) for reg_number in reg_numbers)
	return clause(hatR(address.next(), "V", "Y"), hatR(address, "V", "Y"), "V \= %s" % reg_exclude)

def transfer_all(address):
	if EXPLODE_TRANSFERS:
		reg_numbers = range(address.method.num_regs_total)
		return ''.join(clause(hatR(address.next(), reg_number, "Y"), hatR(address, reg_number, "Y")) for reg_number in reg_numbers)
	return clause(hatR(address.next(), "V", "Y"), hatR(address, "V", "Y"))

def reg_num(address, reg):
	if reg[0] == 'v':
		return int(reg[1:])
	if reg[0] == 'p':
		return int(reg[1:]) + address.method.num_locals
	raise ValueError('Unexpected register name: %s' % reg)
	
def arg_transfer(address, args_registers):
	transfers = ['( V = %d, %s )' % (index, hatR(address, reg_num(address, reg), 'Y')) for index, reg in enumerate(args_registers)]
	out = '(\n\t\t'
	out += ' ;\n\t\t'.join(transfers)
	out += '\n\t)'
	return out

def to_register_list(first_word, registers_raw):
	if first_word.endswith('/range'):
		first_word = first_word[:-6]  # Cut off "/range"
		reg_letter = registers_raw[0]
		assert reg_letter in ('v', 'p')
		first, last = map(int, registers_raw.replace(reg_letter, '').split(' .. '))  # Strip reg_letter and extract numbers
		registers = ['%s%d' % (reg_letter, reg) for reg in range(first, last + 1)]
	else:
		registers = registers_raw.split(', ')
		if registers == ['']:  # Handle the no-arguments case
			registers = []
	return first_word, registers



### Parser and generator functions

def handle_instr_nop(line, first_word, rest):
	Globals.instructions.append_instruction(line, 'nop', [])

def generate_instr_nop(address, data):
	return transfer_all(address)


def handle_instr_const(line, first_word, rest):
	dest, value = rest.split(', ')
	Globals.instructions.append_instruction(line, 'const', [dest, value])

def generate_instr_const(address, data):
	dest, value = data
	if value.endswith('L'):
		value = value[:-1]
		value = quote(value)  # Because XSB can't handle bignums
	out = clause(hatR(address.next(), reg_num(address, dest), value))
	out += transfer_exclude(address, reg_num(address, dest))
	return out


def handle_instr_const_string(line, first_word, rest):
	dest, value = rest.split(', ', 1)
	Globals.instructions.append_instruction(line, 'const-string', [dest, value])

def generate_instr_const_string(address, data):
	dest, value = data
	value = value.replace("'", "''")
	value = value[1:-1]  # remove quotes so it can be concatenated later
	return ''.join([
		clause(hatHnew(quote('Ljava/lang/String;'), address, 'value', quote(value))),
		clause(hatR(address.next(), reg_num(address, dest), "('Ljava/lang/String;', %s, %d)" % (quote(address.method.identifier), address.pc))),
		transfer_exclude(address, reg_num(address, dest)),
		])


def handle_instr_const_class(line, first_word, rest):
	dest, value = rest.split(', ')
	Globals.instructions.append_instruction(line, 'const-class', [dest, value])

def generate_instr_const_class(address, data):
	dest, value = data
	return ''.join([
		clause(hatHnew(quote('Ljava/lang/Class;'), address, 'name', quote(value))),  # TODO: Use a String object
		clause(hatR(address.next(), reg_num(address, dest), "('Ljava/lang/Class;', %s, %d)" % (quote(address.method.identifier), address.pc))),
		transfer_exclude(address, reg_num(address, dest)),
		handle(address, "Ljava/lang/ClassNotFoundException;"),
		handle(address, "Ljava/lang/VerifyError;"),
		])


def handle_instr_const_high16(line, first_word, rest):
	dest, value = rest.split(', ')
	Globals.instructions.append_instruction(line, 'const-high', [dest, value, 16])

def handle_instr_const_wide_high16(line, first_word, rest):
	dest, value = rest.split(', ')
	Globals.instructions.append_instruction(line, 'const-high', [dest, value, 48])

def generate_instr_const_high(address, data):
	dest, value, shift = data
	out = clause(hatR(address.next(), reg_num(address, dest), '%s << %d' % (value, shift)))
	out += transfer_exclude(address, reg_num(address, dest))
	return out


def handle_instr_invoke(line, first_word, rest):

	args_registers_raw, method_identifier = re.match('{([^}]*)}, (.*)', rest).groups()

	first_word, args_registers = to_register_list(first_word, args_registers_raw)

	class_name, method_name, arg_types_raw, return_type_raw = \
			re.match('([^-]*)->([^(]*)\(([^)]*)\)(.*)', method_identifier).groups()
	arg_types = parse_args(arg_types_raw)
	return_type, should_be_empty = parse_arg(return_type_raw)
	if should_be_empty != '':
		raise ValueError('The return type "%s" was not recognized.' % return_type_raw)
	method_signature = MethodSignature(method_identifier, class_name, method_name, arg_types, return_type)
	Globals.method_signatures.append(method_signature)
	Globals.instructions.append_instruction(line, first_word, [args_registers, method_signature])


def handle_exceptions_invoke(address, mid, mid_condition):
	if EXPLODE_TRANSFERS:
		reg_numbers = range(address.method.num_regs_total)
		clauses_transfer_other = (
			"% If handler found, transfer other registers\n"
			+ ''.join(clause(
				hatRraw(quote(address.method.identifier), 'PCH', reg_number, 'Y'),
				"hatE(%s, CLE)" % (mid),
				mid_condition,
				"findHandler(%s, %d, CLE, PCH)" % (quote(address.method.identifier), address.pc),
				"PCH \= bot",
				hatR(address, reg_number, 'Y'),
				) for reg_number in reg_numbers)
			)
	else:
		clauses_transfer_other = clause(
			hatRraw(quote(address.method.identifier), 'PCH', 'V', 'Y'),
			"hatE(%s, CLE)" % (mid),
			mid_condition,
			"findHandler(%s, %d, CLE, PCH)" % (quote(address.method.identifier), address.pc),
			"PCH \= bot",
			hatR(address, 'V', 'Y'),
			)
	return ''.join([
		clause("% If handler found, transfer exception reference to retval",
			hatRraw(quote(address.method.identifier), 'PCH', "retval", 'CLE'),
			"hatE(%s, CLE)" % (mid),
			mid_condition,
			"findHandler(%s, %d, CLE, PCH)" % (quote(address.method.identifier), address.pc),
			"PCH \= bot",
			),
		clauses_transfer_other,
		clause("% If no handler found, add exception to cache",
			"hatE(%s, CLE)" % (quote(address.method.identifier)),
			"hatE(%s, CLE)" % (mid),
			mid_condition,
			"findHandler(%s, %d, CLE, PCH)" % (quote(address.method.identifier), address.pc),
			"PCH = bot",
		),
	])

def generate_instr_invoke_virtual(address, data):
	args_registers, method_signature = data

	# <API>
	if ANALYZE_REFLECTION:
		if method_signature.identifier == 'Ljava/lang/Class;->newInstance()Ljava/lang/Object;':
			return generate_instr_invoke_virtual_Class_newInstance(address, data)
		if method_signature.identifier == 'Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;':
			return generate_instr_invoke_virtual_Class_getMethod(address, data)
		if method_signature.identifier == 'Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;':
			return generate_instr_invoke_virtual_Method_invoke(address, data)

	#    2 clone()Ljava/lang/Object;
	#    2 listIterator()Ljava/util/ListIterator;
	#    3 toString()Ljava/lang/String;
	#    4 ensureCapacity(I)V
	#    6 removeAll(Ljava/util/Collection;)Z
	#   13 toArray()[Ljava/lang/Object;
	#   18 indexOf(Ljava/lang/Object;)I
	#   20 set(ILjava/lang/Object;)Ljava/lang/Object;
	#   30 contains(Ljava/lang/Object;)Z
	#   35 remove(Ljava/lang/Object;)Z							
	#   36 add(ILjava/lang/Object;)V							DONE
	#   37 addAll(Ljava/util/Collection;)Z
	#   47 remove(I)Ljava/lang/Object;							DONE
	#   60 toArray([Ljava/lang/Object;)[Ljava/lang/Object;		DONE
	#   72 isEmpty()Z
	#   79 <init>(Ljava/util/Collection;)V
	#  133 clear()V												DONE
	#  209 <init>(I)V											DONE
	#  378 iterator()Ljava/util/Iterator;
	#  763 size()I												DONE
	#  926 get(I)Ljava/lang/Object;								DONE
	# 1190 add(Ljava/lang/Object;)Z								DONE
	# 1210 <init>()V											DONE


	# TODO: For ArrayList methods: Resolve methodCall destination if subclasses are to be used

	if method_signature.identifier in [
			# 'Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z',
			'Ljava/util/ArrayList;->ensureCapacity(I)V',
			]:
		return transfer_all(address)

	if method_signature.identifier == 'Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;':
		assert len(args_registers) == 2
		reg_arraylist, reg_array = args_registers
		return ''.join([
			clause(
				hatR(address.next(), 'retval', 'ArrRef'),
				hatR(address, reg_num(address, reg_arraylist), "('Ljava/util/ArrayList;', _, _)"),
				hatR(address, reg_num(address, reg_array), "ArrRef"),
				),
			clause(
				hatH("ArrType", "ArrM", "ArrPC", "values", "Y"),
				hatR(address, reg_num(address, reg_array), "(ArrType, ArrM, ArrPC)"),
				hatR(address, reg_num(address, reg_arraylist), "('Ljava/util/ArrayList;', AddrM, AddrPC)"),
				hatH(quote('Ljava/util/ArrayList;'), 'AddrM', 'AddrPC', "array", "Y"),
				),
			transfer_exclude(address, 'retval'),
			handle(address, "Ljava/lang/NullPointerException;"),
			handle(address, "Ljava/lang/IndexOutOfBoundsException;"),
			clause(methodCall(quote(address.method.identifier), quote('API: Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;'))),
			])

	if method_signature.identifier == 'Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;':
		assert len(args_registers) == 2
		reg_arraylist, reg_index = args_registers
		return ''.join([
			clause(
				hatR(address.next(), 'retval', 'Y'),
				hatR(address, reg_num(address, reg_arraylist), "('Ljava/util/ArrayList;', AddrM, AddrPC)"),
				hatH(quote('Ljava/util/ArrayList;'), 'AddrM', 'AddrPC', "array", "Y"),
				),
			transfer_exclude(address, 'retval'),
			handle(address, "Ljava/lang/NullPointerException;"),
			handle(address, "Ljava/lang/IndexOutOfBoundsException;"),
			clause(methodCall(quote(address.method.identifier), quote('API: Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;'))),
			])

	if method_signature.identifier == 'Ljava/util/ArrayList;->clear()V':
		assert len(args_registers) == 1
		return ''.join([
			transfer_all(address),
			handle(address, "Ljava/lang/NullPointerException;"),
			clause(methodCall(quote(address.method.identifier), quote('API: Ljava/util/ArrayList;->clear()V'))),
			])

	if method_signature.identifier == 'Ljava/util/ArrayList;->size()I':
		assert len(args_registers) == 1
		reg_arraylist, = args_registers
		return ''.join([
			clause(
				hatR(address.next(), 'retval', 'top_prim'),
				),
			transfer_exclude(address, 'retval'),
			handle(address, "Ljava/lang/NullPointerException;"),
			clause(methodCall(quote(address.method.identifier), quote('API: Ljava/util/ArrayList;->size()I'))),
			])

	if method_signature.identifier == 'Ljava/util/ArrayList;->get(I)Ljava/lang/Object;':
		assert len(args_registers) == 2
		reg_arraylist, reg_index = args_registers
		return ''.join([
			clause(
				hatR(address.next(), 'retval', 'Y'),
				hatR(address, reg_num(address, reg_arraylist), "('Ljava/util/ArrayList;', AddrM, AddrPC)"),
				hatH(quote('Ljava/util/ArrayList;'), 'AddrM', 'AddrPC', "array", "Y"),
				),
			transfer_exclude(address, 'retval'),
			handle(address, "Ljava/lang/NullPointerException;"),
			handle(address, "Ljava/lang/IndexOutOfBoundsException;"),
			clause(methodCall(quote(address.method.identifier), quote('API: Ljava/util/ArrayList;->get(I)Ljava/lang/Object;'))),
			])

	if method_signature.identifier == 'Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z':
		# TODO: Boxing
		assert len(args_registers) == 2
		reg_arraylist, reg_element = args_registers
		return ''.join([
			clause(
				hatR(address.next(), 'retval', '1'),  # See http://docs.oracle.com/javase/6/docs/api/java/util/ArrayList.html#add%28E%29
				),
			transfer_exclude(address, 'retval'),
			clause(
				hatH(quote('Ljava/util/ArrayList;'), 'AddrM', 'AddrPC', 'array', 'Y'),
				hatR(address, reg_num(address, reg_arraylist), "('Ljava/util/ArrayList;', AddrM, AddrPC)"),
				hatR(address, reg_num(address, reg_element), "Y"),
			),
			handle(address, "Ljava/lang/NullPointerException;"),
			clause(methodCall(quote(address.method.identifier), quote('API: Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z'))),
			])

	if method_signature.identifier == 'Ljava/util/ArrayList;->add(ILjava/lang/Object;)V':
		# TODO: Boxing
		assert len(args_registers) == 3
		reg_arraylist, reg_index, reg_element = args_registers
		return ''.join([
			clause(
				hatH(quote('Ljava/util/ArrayList;'), 'AddrM', 'AddrPC', 'array', 'Y'),
				hatR(address, reg_num(address, reg_arraylist), "('Ljava/util/ArrayList;', AddrM, AddrPC)"),
				hatR(address, reg_num(address, reg_element), "Y"),
			),
			transfer_all(address),
			handle(address, "Ljava/lang/NullPointerException;"),
			handle(address, "Ljava/lang/IndexOutOfBoundsException;"),
			clause(methodCall(quote(address.method.identifier), quote('API: Ljava/util/ArrayList;->add(ILjava/lang/Object;)V'))),
			])

	if method_signature.identifier == 'Landroid/database/Cursor;->getString(I)Ljava/lang/String;':
		assert len(args_registers) == 2
		# Ignores the cursor and returns a tainted value
		# reg_cursor, reg_index = args_registers
		return ''.join([
			clause(hatHnew(quote('Ljava/lang/String;'), address, 'value', 'tainted_Cursor_getString')),
			clause(hatR(address.next(), "retval", "('Ljava/lang/String;', %s, %d)" % (quote(address.method.identifier), address.pc))),
			transfer_exclude(address, 'retval'),
			handle(address, "Ljava/lang/NullPointerException;"),
			# TODO: Resolve methodCall destination
			# clause(methodCall(quote(address.method.identifier), quote('API: Landroid/database/Cursor;->getString(I)Ljava/lang/String;'))),
			])
	# </API>

	return ''.join([
			clause("% Transfer arguments into invoked method",
				"invoke(MID, V, Y)",
				hatR(address, reg_num(address, args_registers[0]), '(CL, _, _)'),
				"objref(CL)",
				"resolve(CL, %s, %s, %s, MID)" % (
					quote(method_signature.method_name),
					method_signature.arg_types,
					quote(method_signature.return_type),
					),
				arg_transfer(address, args_registers),
				), 
			clause("% Move return value to retval",
				hatR(address.next(), 'retval', 'Y'),
				hatR(address, reg_num(address, args_registers[0]), '(CL, _, _)'),
				"objref(CL)",
				"resolve(CL, %s, %s, %s, MID)" % (
					quote(method_signature.method_name),
					method_signature.arg_types,
					quote(method_signature.return_type),
					),
				"return(MID, Y)",
				) if method_signature.return_type != 'V' else '',
			transfer_exclude(address, 'retval'),
			# Exceptions
			handle_exceptions_invoke(address, 'MID', ',\n\t'.join([
				hatR(address, reg_num(address, args_registers[0]), '(CL, _, _)'),
				"objref(CL)",
				"resolve(CL, %s, %s, %s, MID)" % (
					quote(method_signature.method_name),
					method_signature.arg_types,
					quote(method_signature.return_type),
					),
			])) if RUNTIME_EXCEPTIONS else '',
			# End of exceptions
			clause("% Call graph",
				methodCall(quote(address.method.identifier), 'MIDdest'),
				hatR(address, reg_num(address, args_registers[0]), '(CL, _, _)'),
				"objref(CL)",
				"resolve(CL, %s, %s, %s, MIDdest)" % (
					quote(method_signature.method_name),
					method_signature.arg_types,
					quote(method_signature.return_type),
					),
				),
			])


generate_instr_invoke_interface = generate_instr_invoke_virtual


def generate_instr_invoke_virtual_Class_newInstance(address, data):  # REFLECTION
	# TODO: Call the parameterless constructor
	args_registers, method_signature = data
	args_register, = args_registers
	return ''.join([
			clause(
				hatR(address.next(), 'retval', "(NewCL, %s, %d)" % (quote(address.method.identifier), address.pc)),
				hatR(address, reg_num(address, args_register), "('Ljava/lang/Class;', AddrMID, AddrPC)"),
				hatH(quote('Ljava/lang/Class;'), 'AddrMID', 'AddrPC', 'name', 'NewCL'),
				),
			# # This clause makes simple hatH lookups fail with this message for no apparent reason:
			# # ++Error[XSB/Runtime/P]: [Miscellaneous]  [PCRE:SUBSTITUTE] Arg 4 (the output) must be an unbound variable.
			# clause("% Create a class for each instantiated (API) class",
			# 	"class(NewCL)",
			# 	hatR(address, reg_num(address, args_register), "('Ljava/lang/Class;', AddrMID, AddrPC)"),
			# 	hatH(quote('Ljava/lang/Class;'), 'AddrMID', 'AddrPC', 'name', 'NewCL'),
			# 	),
			clause("% Call graph",
				methodCall(quote(address.method.identifier), quote('API: Ljava/lang/Class;->newInstance()Ljava/lang/Object;')),
				),
			transfer_exclude(address, 'retval'),
			])


def generate_instr_invoke_virtual_Class_getMethod(address, data):  # REFLECTION
	args_registers, method_signature = data

	class_reg, method_name_reg, types_reg = args_registers
	return ''.join([
			clause(
				hatR(address.next(), 'retval', "('Ljava/lang/reflect/Method;', %s, %d)" % (quote(address.method.identifier), address.pc)),
				),
			clause(
				hatHnew(quote('Ljava/lang/reflect/Method;'), address, 'declaringClass', 'Y'), 
				"( " + 
					hatR(address, reg_num(address, class_reg), "('Ljava/lang/Class;', CLAddrMID, CLAddrPC)"),
					hatH(quote('Ljava/lang/Class;'), 'CLAddrMID', 'CLAddrPC', 'name', 'CLFrom'),
					hatHnew(quote('Ljava/lang/reflect/Method;'), address, 'name', 'MName'), 
					"resolveFact(CLFrom, MName, _, _, MID), method(MID, Y, _, _, _, _, _)"
				+ " ) ; ( " + 
					hatR(address, reg_num(address, class_reg), "(top_ref, analysis, 0)"),
					"Y = top_class_name )",
				),
			clause(
				hatHnew(quote('Ljava/lang/reflect/Method;'), address, 'name', 'Y'), 
				"( " + hatR(address, reg_num(address, method_name_reg), "('Ljava/lang/String;', StrAddrMID, StrAddrPC)"),
				hatH(quote('Ljava/lang/String;'), 'StrAddrMID', 'StrAddrPC', 'value', 'Y') + " ) ; ( " + hatR(address, reg_num(address, method_name_reg), "(top_ref, analysis, 0)"),
					"Y = top_method_name )",
				),
			clause(
				"reflection('clazz.getMethod.clazzName', %s, %d, Y)" % (quote(address.method.identifier), address.pc),
				"( " + hatR(address, reg_num(address, class_reg), "('Ljava/lang/Class;', CLAddrMID, CLAddrPC)"),
				hatH(quote('Ljava/lang/Class;'), 'CLAddrMID', 'CLAddrPC', 'name', 'Y') + " ) ; ( " + hatR(address, reg_num(address, class_reg), "(top_ref, analysis, 0)"),
					"Y = top_class_name )",
				),
			clause(
				"reflection('clazz.getMethod.methodName', %s, %d, Y)" % (quote(address.method.identifier), address.pc),
				"( " + hatR(address, reg_num(address, method_name_reg), "('Ljava/lang/String;', StrAddrMID, StrAddrPC)"),
				hatH(quote('Ljava/lang/String;'), 'StrAddrMID', 'StrAddrPC', 'value', 'Y') + " ) ; ( " + hatR(address, reg_num(address, method_name_reg), "(top_ref, analysis, 0)"),
					"Y = top_method_name )",
				),
			clause("% Call graph",
				methodCall(quote(address.method.identifier), quote('API: Ljava/lang/Class;->getMethod(Ljava/lang/String;)Ljava/lang/reflect/Method;')),
				),
			transfer_exclude(address, 'retval'),
			])


def generate_instr_invoke_virtual_Method_invoke(address, data):  # REFLECTION
	args_registers, method_signature = data
	method_obj_reg, receiver_obj_reg, args_reg = args_registers
	# TODO static invokes with Method.invoke
	return ''.join([
			clause(
				"reflectInvoke(MID, _, Y)",
				hatR(address, reg_num(address, method_obj_reg), "('Ljava/lang/reflect/Method;', AddrMM, AddrPCM)"),
				hatH(quote('Ljava/lang/reflect/Method;'), 'AddrMM', 'AddrPCM', 'name', 'MethodName'),
				hatR(address, reg_num(address, receiver_obj_reg), "(CLR, _, _)"),
				"resolve(CLR, MethodName, _, _, MID)",
				hatR(address, reg_num(address, args_reg), "(ArrayType, AddrMA, AddrPCA)"),
				hatH("ArrayType", 'AddrMA', 'AddrPCA', 'values', 'Y')
				),
			# Exceptions
			handle_exceptions_invoke(address, 'MID', ',\n\t'.join([
				hatR(address, reg_num(address, method_obj_reg), "('Ljava/lang/reflect/Method;', AddrMM, AddrPCM)"),
				hatH(quote('Ljava/lang/reflect/Method;'), 'AddrMM', 'AddrPCM', 'name', 'MethodName'),
				hatR(address, reg_num(address, receiver_obj_reg), "(CLR, _, _)"),
				"resolve(CLR, MethodName, _, _, MID)",
			])) if RUNTIME_EXCEPTIONS else '',
			# End of exceptions
			clause("% Call graph",
				methodCall(quote(address.method.identifier), 'MID', 'reflection'), 
				hatR(address, reg_num(address, method_obj_reg), "('Ljava/lang/reflect/Method;', AddrMM, AddrPCM)"),
				hatH(quote('Ljava/lang/reflect/Method;'), 'AddrMM', 'AddrPCM', 'name', 'MethodName'),
				hatR(address, reg_num(address, receiver_obj_reg), "(CLR, _, _)"),
				"resolve(CLR, MethodName, _, _, MID)",
				),
			transfer_exclude(address, 'retval'),
			])


def invoke_resolved(address, args_registers, method_signature, kind, MID):
	out = []
	if MID != quote(UNRESOLVED_MID):
		# Find NumLocals
		NumLocals = False
		for method in Globals.methods:
			if quote(method.identifier) == MID:
				NumLocals = method.num_locals
				break
		if NumLocals is False:
			print >> sys.stderr, 'NumLocals was not found for method: ' + MID
			sys.exit(1)

		out.append("% Transfer arguments (directly) into invoked method\n")

		regs_out = []
		for index, reg in enumerate(args_registers, NumLocals):
			out.append(clause(hatRraw(MID, 0, index, 'Y'),
				hatR(address, reg_num(address, reg), 'Y')))
		if method_signature.return_type != 'V':
			out.append(clause("% Move return value to retval",
				hatR(address.next(), 'retval', 'Y'),
				"return(%s, Y)" % MID,
				))
		# Exceptions
		if RUNTIME_EXCEPTIONS:
			out.append(handle_exceptions_invoke(address, MID, ''))

	else:  # Unresolved method
		args_registers_with_ref_types = []
		arg_types = ([] if kind == 'static' else [method_signature.clazz]) + method_signature.arg_types
		for index, arg_type in enumerate(arg_types):
			if is_possibly_mutable_reference_type(arg_type):
				args_registers_with_ref_types.append((index, args_registers[index]))
		for index, reg in args_registers_with_ref_types:
			out.append(clause("invoke(%s, %d, Y)" % (MID, index),
				   hatR(address, reg_num(address, reg), 'Y')))
		out.append(clause("% Move return value to retval",
				hatR(address.next(), 'retval', '(top_ref, analysis, 0)'),
				))
	out.append(transfer_exclude(address, 'retval'))
	out.append(clause("% Call graph",
			methodCall(quote(address.method.identifier), MID),
			))
	return ''.join(out)


def generate_invoke_static_Class_forName(address, args_registers, method_signature):  # REFLECTION
	args_register , = args_registers
	return ''.join([
			clause("% Transfer strings into the Class object's name field",
				hatHnew(quote('Ljava/lang/Class;'), address, 'name', 'Y'),
				"( " + hatR(address, reg_num(address, args_register), "('Ljava/lang/String;', AddrMID, AddrPC)"),
				hatH(quote('Ljava/lang/String;'), 'AddrMID', 'AddrPC', 'value', 'Ytemp'),
				"fmt_write_string(Ytemp2, 'L%s;', Ytemp)", 
				"pcre:substitute('\.', Ytemp2, '/', Y)" + " ) ; ( " + hatR(address, reg_num(address, args_register), "(top_ref, analysis, 0)") + ", Y = top_class_name )",
				),
			clause(
				hatR(address.next(), 'retval', "('Ljava/lang/Class;', %s, %d)" % (quote(address.method.identifier), address.pc)),
				),
			transfer_exclude(address, 'retval'),
			clause(
				"reflection('Class.forName', %s, %d, Y)" % (quote(address.method.identifier), address.pc),
				"( " + hatR(address, reg_num(address, args_register), "('Ljava/lang/String;', AddrMID, AddrPC)"),
				hatH(quote('Ljava/lang/String;'), 'AddrMID', 'AddrPC', 'value', 'Ytemp'),
				"fmt_write_string(Ytemp2, 'L%s;', Ytemp)", 
				"pcre:substitute('\.', Ytemp2, '/', Y)" + " ) ; ( " + hatR(address, reg_num(address, args_register), "(top_ref, analysis, 0)") + ", Y = top_class_name )",
				),
			clause("% Call graph",
				methodCall(quote(address.method.identifier), quote('API: Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;')),
				),
			])

def generate_instr_invoke_direct(address, data):
	args_registers, method_signature = data

	# <API>
	if method_signature.identifier == 'Ljava/lang/Object;-><init>()V':
		return ''.join([
			transfer_all(address),
			clause("% Call graph",
				methodCall(quote(address.method.identifier), quote('API: Ljava/lang/Object;-><init>()V')),
			)
			])

	if method_signature.identifier == 'Ljava/util/ArrayList;-><init>()V':
		return ''.join([
			transfer_all(address),
			clause("% Call graph",
				methodCall(quote(address.method.identifier), quote('API: Ljava/util/ArrayList;-><init>()V')),
			)
			])

	if method_signature.identifier == 'Ljava/util/ArrayList;-><init>(I)V':
		return ''.join([
			transfer_all(address),
			clause("% Call graph",
				methodCall(quote(address.method.identifier), quote('API: Ljava/util/ArrayList;-><init>(I)V')),
			)
			])
	# </API>

	MID = UNRESOLVED_MID
	for method in Globals.methods:
		if method.identifier == method_signature.identifier:
			MID = method.identifier
			break
	MID = quote(MID)

	return invoke_resolved(address, args_registers, method_signature, 'direct', MID)



def generate_instr_invoke_super(address, data):
	args_registers, method_signature = data

	return ''.join([
			clause("% Transfer arguments into invoked method",
				"invoke(MID, V, Y)",
				hatR(address, reg_num(address, args_registers[0]), '(CL, _, _)'),
				"objref(CL)",
				"super(P, CL)",
				"resolve(P, %s, %s, %s, MID)" % (
					quote(method_signature.method_name),
					method_signature.arg_types,
					quote(method_signature.return_type),
					),
				arg_transfer(address, args_registers),
				), 
			clause("% Move return value to retval",
				hatR(address.next(), 'retval', 'Y'),
				hatR(address, reg_num(address, args_registers[0]), '(CL, _, _)'),
				"objref(CL)",
				"super(P, CL)",
				"resolve(P, %s, %s, %s, MID)" % (
					quote(method_signature.method_name),
					method_signature.arg_types,
					quote(method_signature.return_type),
					),
				"return(MID, Y)",
				) if method_signature.return_type != 'V' else '',
			transfer_exclude(address, 'retval'),
			# Exceptions
			handle_exceptions_invoke(address, 'MID', ',\n\t'.join([
				hatR(address, reg_num(address, args_registers[0]), '(CL, _, _)'),
				"objref(CL)",
				"super(P, CL)",
				"resolve(P, %s, %s, %s, MID)" % (
					quote(method_signature.method_name),
					method_signature.arg_types,
					quote(method_signature.return_type),
					),
			])) if RUNTIME_EXCEPTIONS else '',
			# End of exceptions
			clause("% Call graph",
				methodCall(quote(address.method.identifier), 'MIDdest'),
				hatR(address, reg_num(address, args_registers[0]), '(CL, _, _)'),
				"objref(CL)",
				"super(P, CL)",
				"resolve(P, %s, %s, %s, MIDdest)" % (
					quote(method_signature.method_name),
					method_signature.arg_types,
					quote(method_signature.return_type),
					),
				),
			])


def generate_static_valueof(address, data, clazz):
	args_registers, method_signature = data

	return ''.join([
		clause("% API call, create " + clazz +" object",
			hatR(address.next(), 'retval', "(%s, %s, %s)" % (quote(clazz), quote(address.method.identifier), address.pc)),
		),
		clause(
			hatHnew(quote(clazz), address, 'value', 'Y'),
			hatR(address, reg_num(address, args_registers[0]), 'Y')
		),
		transfer_exclude(address, 'retval'),
		clause("% Call graph",
			methodCall(quote(address.method.identifier), quote('API: ' + method_signature.identifier)),
		)
		])


def generate_instr_invoke_static(address, data):
	args_registers, method_signature = data

	if ANALYZE_REFLECTION and method_signature.identifier == 'Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;':
		return generate_invoke_static_Class_forName(address, args_registers, method_signature)
	
	valueOfSignatures = [
				'Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;',
				'Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;',
				'Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;',
				'Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;',
				'Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;',
				'Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;', 
				'Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;', 
				'Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;',
			]
	if method_signature.identifier in valueOfSignatures:
		return generate_static_valueof(address, data, method_signature.clazz)

	key = method_signature.clazz + method_signature.method_name + str(method_signature.arg_types) + method_signature.return_type + 'static'

	MID = Globals.resolve_fact_map.get(key) or UNRESOLVED_MID
	MID = quote(MID)
	if quote(address.method.identifier) == MID:
		print >> sys.stderr, 'NOTICE: Statically detected recursion in %s' % MID

	return invoke_resolved(address, args_registers, method_signature, 'static', MID)



def handle_instr_return(line, first_word, rest):
	Globals.instructions.append_instruction(line, 'return', [rest])

def generate_instr_return(address, data):
	return clause(
		"return(%s, Y)" % quote(address.method.identifier),
		hatR(address, reg_num(address, data[0]), 'Y'),
		)


def handle_instr_return_void(line, first_word, rest):
	Globals.instructions.append_instruction(line, 'return-void', [])


def handle_instr_move(line, first_word, rest):
	dest, src = rest.split(', ')
	Globals.instructions.append_instruction(line, 'move', [dest, src])

def generate_instr_move(address, data):
	dest, src = data
	return ''.join([
		clause(
			hatR(address.next(), reg_num(address, dest), "Y"),
			hatR(address, reg_num(address, src), "Y"),
		),
		transfer_exclude(address, reg_num(address, dest)),
		])



def handle_instr_move_result(line, first_word, rest):
	Globals.instructions.append_instruction(line, 'move-result', [rest])

def handle_instr_move_exception(line, first_word, rest):
	Globals.instructions.append_instruction(line, 'move-exception', [rest])

def generate_instr_move_result(address, data):
	dest = data[0]
	return ''.join([
		clause(
			hatR(address.next(), reg_num(address, dest), "Y"),
			hatR(address, "retval", "Y"),
		),
		transfer_exclude(address, reg_num(address, dest)),
		])


def handle_instr_unop(line, first_word, rest):
	dest, src = rest.split(', ')
	Globals.instructions.append_instruction(line, 'unop', [first_word, dest, src])

def generate_instr_unop(address, data):
	op, dest, src = data
	return ''.join([
		clause(
		hatR(address.next(), reg_num(address, dest), 'Y'),
		hatR(address, reg_num(address, src), 'A'),
		"unop(%s, A, Y)" % quote(op),
		),
		transfer_exclude(address, reg_num(address, dest)),
		handle(address, "Ljava/lang/ArithmeticException;"),
		])


def handle_instr_binop(line, first_word, rest):
	dest, src1, src2 = rest.split(', ')
	Globals.instructions.append_instruction(line, 'binop', [first_word, dest, src1, src2])

def generate_instr_binop(address, data):
	op, dest, src1, src2 = data
	return ''.join([
		clause(
		hatR(address.next(), reg_num(address, dest), 'Y'),
		hatR(address, reg_num(address, src1), 'A'),
		hatR(address, reg_num(address, src2), 'B'),
		"binop(%s, A, B, Y)" % quote(op),
		),
		transfer_exclude(address, reg_num(address, dest)),
		handle(address, "Ljava/lang/ArithmeticException;"),
		])

def handle_instr_binop_2addr(line, first_word, rest):
	dest_src1, src2 = rest.split(', ')
	operation = first_word.replace('/2addr', '')
	Globals.instructions.append_instruction(line, 'binop/2addr', [operation, dest_src1, src2])

def generate_instr_binop_2addr(address, data):
	op, dest_src1, src2 = data
	return ''.join([
		clause(
			hatR(address.next(), reg_num(address, dest_src1), 'Y'),
			hatR(address, reg_num(address, dest_src1), 'A'),
			hatR(address, reg_num(address, src2), 'B'),
			"binop(%s, A, B, Y)" % quote(op),
		),
		transfer_exclude(address, reg_num(address, dest_src1)),
		handle(address, "Ljava/lang/ArithmeticException;"),
		])

def handle_instr_binop_lit(line, first_word, rest):
	dest, src, lit = rest.split(', ')
	operation = first_word.replace('/lit8', '').replace('/lit16', '')
	Globals.instructions.append_instruction(line, 'binop/lit', [operation, dest, src, lit])

def generate_instr_binop_lit(address, data):
	op, dest, src, lit = data
	return ''.join([
		clause(
			hatR(address.next(), reg_num(address, dest), 'Y'),
			hatR(address, reg_num(address, src), 'A'),
			"binop(%s, A, %s, Y)" % (quote(op), lit),
		),
		transfer_exclude(address, reg_num(address, dest)),
		handle(address, "Ljava/lang/ArithmeticException;"),
		])


def handle_instr_if(line, first_word, rest):
	src1, src2, label = rest.split(', ')
	Globals.instructions.append_instruction(line, 'if', [first_word, src1, src2, label])

def handle_instr_ifz(line, first_word, rest):
	src, label = rest.split(', ')
	Globals.instructions.append_instruction(line, 'ifz', [first_word, src, label])

def generate_instr_if(address, data):
	jump_dest = label_lookup(data[-1], address.method)
	out = StringIO()
	if int(jump_dest) < address.pc:
		Globals.num_backedges += 1
		out.write('% "if" with back-edge\n')

	reg_numbers = range(address.method.num_regs_total)
	out.write(''.join(clause(
		hatRraw(quote(address.method.identifier), jump_dest, reg_number, 'Y'),
		hatR(address, reg_number, 'Y'),
		) for reg_number in reg_numbers)
		)

	out.write(transfer_all(address))
	out_ = out.getvalue()
	out.close()
	return out_


def handle_instr_cmp(line, first_word, rest):
	dest, src1, src2 = rest.split(', ')
	Globals.instructions.append_instruction(line, 'cmp', [first_word, dest, src1, src2])

def generate_instr_cmp(address, data):
	bias, dest, src1, src2 = data
	return ''.join([
		clause(hatR(address.next(), reg_num(address, dest), -1)),
		clause(hatR(address.next(), reg_num(address, dest), 0)),
		clause(hatR(address.next(), reg_num(address, dest), 1)),
		transfer_exclude(address, reg_num(address, dest)),
		])


def handle_instr_goto(line, first_word, rest):
	Globals.instructions.append_instruction(line, 'goto', [rest])

def generate_instr_goto(address, data):
	jump_dest = label_lookup(data[0], address.method)
	if int(jump_dest) < address.pc:
		Globals.num_backedges += 1

	reg_numbers = range(address.method.num_regs_total)
	return ''.join(clause(
		hatRraw(quote(address.method.identifier), jump_dest, reg_number, 'Y'),
		hatR(address, reg_number, 'Y'),
		) for reg_number in reg_numbers)


def handle_instr_new_instance(line, first_word, rest):
	dest, clazz = rest.split(', ')
	Globals.instructions.append_instruction(line, 'new-instance', [dest, clazz])

def generate_instr_new_instance(address, data):
	dest, clazz = data
	return ''.join([
		clause(
			hatR(address.next(), reg_num(address, dest), "(%s, %s, %d)" % (quote(clazz), quote(address.method.identifier), address.pc)),
		),
		transfer_exclude(address, reg_num(address, dest)),
		clause("% Create a class fact for each instantiated (API) class",
			"class(%s)" % quote(clazz),
		),
		])


def handle_instr_monitor(line, first_word, rest):
	Globals.instructions.append_instruction(line, first_word, [rest])

def generate_instr_monitor(address, data):
	return ''.join([
		transfer_all(address),
		handle(address, "Ljava/lang/NullPointerException;"),
		handle(address, "Ljava/lang/IllegalMonitorStateException;"),
		])


def handle_instr_check_cast(line, first_word, rest):
	src, clazz = rest.split(', ')
	Globals.instructions.append_instruction(line, 'check-cast', [src, clazz])

def generate_instr_check_cast(address, data):
	return transfer_all(address) + handle(address, "Ljava/lang/ClassCastException;")

def handle_instr_throw(line, first_word, rest):
	Globals.instructions.append_instruction(line, 'throw', [rest])

def generate_instr_throw(address, data):
	src, = data

	PCH = "PCH"
	return ''.join([
		clause("% If handler found, transfer exception reference to retval",
				hatRraw(quote(address.method.identifier), PCH, "retval", "CLE"),
				hatR(address, reg_num(address, src), "CLE"),
				"findHandler(%s, %d, %s, %s)" % (quote(address.method.identifier), address.pc, "CLE", PCH),
				"PCH \= bot",
				),
		clause("% If handler found, transfer other registers",
				hatRraw(quote(address.method.identifier), PCH, 'V', 'Y'),
				hatR(address, reg_num(address, src), "CLE"),
				"findHandler(%s, %d, %s, %s)" % (quote(address.method.identifier), address.pc, "CLE", PCH),
				"PCH \= bot",
				hatR(address, 'V', 'Y'),
				'V \= retval',
				),
		clause("% If no handler found, add exception to cache",
				"hatE(%s, %s)" % (quote(address.method.identifier), "CLE"),
				hatR(address, reg_num(address, src), "CLE"),
				"findHandler(%s, %d, %s, PCH)" % (quote(address.method.identifier), address.pc, "CLE"),
				"PCH = bot",
				),
		handle(address, "Ljava/lang/NullPointerException;"),
		])


def handle_instr_instance_of(line, first_word, rest):
	dest, src, clazz = rest.split(', ')
	Globals.instructions.append_instruction(line, 'instance-of', [dest, src, clazz])

def generate_instr_instance_of(address, data):
	dest, src, clazz = data
	return ''.join([
		clause( hatR(address.next(), reg_num(address, dest), "0")),
		clause( hatR(address.next(), reg_num(address, dest), "1")),
		transfer_exclude(address, reg_num(address, dest)),
		])


def handle_instr_iget(line, first_word, rest):
	dest, src, field_raw = rest.split(', ')
	clazz, field_name = re.match('(L[^;]*;)->([^:]*):.*', field_raw).groups()
	Globals.instructions.append_instruction(line, 'iget', [dest, src, clazz, field_name])

def generate_instr_iget(address, data):
	dest, src, clazz, field_name = data
	return ''.join([
		clause(
			hatR(address.next(), reg_num(address, dest), "Y"),
			hatR(address, reg_num(address, src), "(CL, CLAddrMID, CLAddrPC)"),
			"ancestor(%s, CL)" % quote(clazz),
			hatH('CL', 'CLAddrMID', 'CLAddrPC', field_name, 'Y'),
		),
		transfer_exclude(address, reg_num(address, dest)),
		handle(address, "Ljava/lang/NullPointerException;"),
		handle(address, "Ljava/lang/IllegalAccessException;"),
		])

def handle_instr_iput(line, first_word, rest):
	src, dest, field_raw = rest.split(', ')
	clazz, field_name = re.match('(L[^;]*;)->([^:]*):.*', field_raw).groups()
	Globals.instructions.append_instruction(line, 'iput', [src, dest, clazz, field_name])

def generate_instr_iput(address, data):
	src, dest, clazz, field_name = data
	return ''.join([
		clause(
			hatH('CL', 'CLAddrMID', 'CLAddrPC', field_name, 'Y'),
			hatR(address, reg_num(address, dest), "(CL, CLAddrMID, CLAddrPC)"),
			"subclass(CL, %s)" % quote(clazz),
			hatR(address, reg_num(address, src), "Y"),
		),
		transfer_all(address),
		handle(address, "Ljava/lang/NullPointerException;"),
		handle(address, "Ljava/lang/IllegalAccessException;"),
		])


def handle_instr_sget(line, first_word, rest):
	dest, field_raw = rest.split(', ')
	clazz, field_name = re.match('(L[^;]*;)->(.*)', field_raw).groups()
	Globals.instructions.append_instruction(line, 'sget', [dest, clazz, field_name])

def generate_instr_sget(address, data):
	dest, clazz, field_name = data
	return ''.join([
		clause(
			hatR(address.next(), reg_num(address, dest), "Y"),
			"hatS(%s, %s, Y)" % (quote(clazz), quote(field_name)),
		),
		transfer_exclude(address, reg_num(address, dest)),
		handle(address, "Ljava/lang/NullPointerException;"),
		handle(address, "Ljava/lang/IllegalAccessException;"),
		])

def handle_instr_sput(line, first_word, rest):
	src, field_raw = rest.split(', ')
	clazz, field_name = re.match('(L[^;]*;)->(.*)', field_raw).groups()
	Globals.instructions.append_instruction(line, 'sput', [src, clazz, field_name])

def generate_instr_sput(address, data):
	src, clazz, field_name = data
	return ''.join([
		clause(
			"hatS(%s, %s, Y)" % (quote(clazz), quote(field_name)),
			hatR(address, reg_num(address, src), "Y"),
		),
		transfer_all(address),
		handle(address, "Ljava/lang/NullPointerException;"),
		handle(address, "Ljava/lang/IllegalAccessException;"),
		])


def handle_instr_switch(line, first_word, rest):
	reg, switch_label = rest.split(' ')
	Globals.instructions.append_instruction(line, first_word, [reg, switch_label])

def generate_instr_switch(address, data):
	switch_label = data[1]
	out = StringIO()
	switch_table = None
	for table in address.method.switches:
		if table.label[0] == switch_label:
			switch_table = table
			break

	for (_, switch_dest_label) in switch_table.mapping:
		jump_dest = label_lookup(switch_dest_label, address.method)
		if int(jump_dest) < address.pc:
			Globals.num_backedges += 1
		out.write(clause(
		hatRraw(quote(address.method.identifier), jump_dest, "V", "Y"),
		hatR(address, "V", "Y"),
		))

	out.write(transfer_all(address))
	out_ = out.getvalue()
	out.close()
	return out_

def handle_instr_new_array(line, first_word, rest):
	dest, length_reg, arr_type = rest.split(', ')
	Globals.instructions.append_instruction(line, 'new-array', [dest, length_reg, arr_type])

def generate_instr_new_array(address, data):
	dest, _, arr_type = data
	return ''.join([
		clause(
			"arrref(%s)" % quote(arr_type)
		),
		clause(
			hatHnew(quote(arr_type), address, 'values', 0),
		),
		clause(
			hatR(address.next(), reg_num(address, dest), "(%s, %s, %d)" % (quote(arr_type), quote(address.method.identifier), address.pc))
		),
		transfer_exclude(address, reg_num(address, dest)),
		handle(address, "Ljava/lang/NegativeArraySizeException;"),
		])

def handle_instr_aput(line, first_word, rest):
	val_reg, arr_reg, index_reg = rest.split(', ')
	Globals.instructions.append_instruction(line, 'aput', [val_reg, arr_reg, index_reg])

def generate_instr_aput(address, data):
	val_reg, arr_reg, _ = data
	return ''.join([
		clause(
			hatH('ArrType', 'ArrTypeAddrMID', 'ArrTypeAddrPC', 'values', 'Y'),
			hatR(address, reg_num(address, arr_reg), "(ArrType, ArrTypeAddrMID, ArrTypeAddrPC)"),
			"arrref(ArrType)",
			hatR(address, reg_num(address, val_reg), "Y")
		),
		transfer_all(address),
		handle(address, "Ljava/lang/NullPointerException;"),
		handle(address, "Ljava/lang/ArrayIndexOutOfBoundsException;"),
		])

def handle_instr_aget(line, first_word, rest):
	dest_reg, arr_reg, index_reg = rest.split(', ')
	Globals.instructions.append_instruction(line, 'aget', [dest_reg, arr_reg, index_reg])

def generate_instr_aget(address, data):
	dest_reg, arr_reg, _ = data
	return ''.join([
		clause(
			hatR(address.next(), reg_num(address, dest_reg), "Y"),
			hatR(address, reg_num(address, arr_reg), "(ArrType, ArrTypeAddrMID, ArrTypeAddrPC)"),
			"arrref(ArrType)",
			hatH('ArrType', 'ArrTypeAddrMID', 'ArrTypeAddrPC', 'values', 'Y'),
		),
		transfer_exclude(address, reg_num(address, dest_reg)),
		handle(address, "Ljava/lang/NullPointerException;"),
		handle(address, "Ljava/lang/ArrayIndexOutOfBoundsException;"),
		])

def handle_instr_fill_array_data(line, first_word, rest):
	reg, label = rest.split(', ')
	Globals.instructions.append_instruction(line, 'fill-array-data', [reg, label])

def generate_instr_fill_array_data(address, data):
	reg, label = data
	ys = ['(Y = %s)' % quote(y) for y in address.method.array_data[label]]
	return ''.join([
		clause(
				hatH('ArrType', 'ArrTypeAddrMID', 'ArrTypeAddrPC', 'values', 'Y'),
				hatR(address, reg_num(address, reg), "(ArrType, ArrTypeAddrMID, ArrTypeAddrPC)"),
				"arrref(ArrType)",
				(
					"(\n\t\t" +
					" ;\n\t\t".join(ys) +
					"\n\t)"
				),
			),
		transfer_all(address),
		handle(address, "Ljava/lang/NullPointerException;"),
		])


def handle_instr_array_length(line, first_word, rest):
	dest, array_reg = rest.split(', ')
	Globals.instructions.append_instruction(line, 'array-length', [dest, array_reg])

def generate_instr_array_length(address, data):
	dest, _ = data
	return ''.join([
			clause( hatR(address.next(), reg_num(address, dest), "top_prim"),),
			transfer_exclude(address, reg_num(address, dest)),
			handle(address, "Ljava/lang/NullPointerException;"),
			])

def handle_instr_filled_new_array(line, first_word, rest):
	val_registers_raw, array_type = re.match('{([^}]*)}, (.*)', rest).groups()

	first_word, val_registers = to_register_list(first_word, val_registers_raw)

	Globals.instructions.append_instruction(line, first_word, [val_registers, array_type])

def generate_instr_filled_new_array(address, data):
	val_regs, array_type = data
	ys = [hatR(address, reg_num(address, v), 'Y') for v in val_regs]
	return ''.join([
		clause(
			hatR(address.next(), 'retval', "(%s, %s, %d)" % (quote(array_type), quote(address.method.identifier), address.pc))
		),
		clause(
			"arrref(%s)" % quote(array_type)
		),
		(clause(
			hatHnew(quote(array_type), address, 'values', 'Y'),
			(
				"(\n\t\t" +
				" ;\n\t\t".join(ys) +
				"\n\t)"
			),
		) if len(val_regs) else ''),
		transfer_exclude(address, 'retval'),
		# handle(address, "Ljava/lang/NegativeArraySizeException;"),   # wtf @ docs?
		])


instruction_dict = {
		'nop'                       : handle_instr_nop,
		'move'                      : handle_instr_move,
		'move/from16'               : handle_instr_move,
		'move/16'                   : handle_instr_move,
		'move-wide'                 : handle_instr_move,
		'move-wide/from16'          : handle_instr_move,
		'move-wide/16'              : handle_instr_move,
		'move-object'               : handle_instr_move,
		'move-object/from16'        : handle_instr_move,
		'move-object/16'            : handle_instr_move,
		'move-result'               : handle_instr_move_result,
		'move-result-wide'          : handle_instr_move_result,
		'move-result-object'        : handle_instr_move_result,
		'move-exception'            : handle_instr_move_exception,
		'return-void'               : handle_instr_return_void,
		'return'                    : handle_instr_return,
		'return-wide'               : handle_instr_return,
		'return-object'             : handle_instr_return,
		'const/4'                   : handle_instr_const,
		'const/16'                  : handle_instr_const,
		'const'                     : handle_instr_const,
		'const/high16'              : handle_instr_const_high16,
		'const-wide/16'             : handle_instr_const,
		'const-wide/32'             : handle_instr_const,
		'const-wide'                : handle_instr_const,
		'const-wide/high16'         : handle_instr_const_wide_high16,
		'const-string'              : handle_instr_const_string,
		'const-string/jumbo'        : handle_instr_const_string,
		'const-class'               : handle_instr_const_class,
		'monitor-enter'             : handle_instr_monitor,
		'monitor-exit'              : handle_instr_monitor,
		'check-cast'                : handle_instr_check_cast,
		'instance-of'               : handle_instr_instance_of,
		'array-length'              : handle_instr_array_length,
		'new-instance'              : handle_instr_new_instance,
		'new-array'                 : handle_instr_new_array,
		'filled-new-array'          : handle_instr_filled_new_array,
		'filled-new-array/range'    : handle_instr_filled_new_array,
		'fill-array-data'           : handle_instr_fill_array_data,
		'throw'                     : handle_instr_throw,
		'goto'                      : handle_instr_goto,
		'goto/16'                   : handle_instr_goto,
		'goto/32'                   : handle_instr_goto,
		'packed-switch'             : handle_instr_switch,
		'sparse-switch'             : handle_instr_switch,
		'cmpl-float'                : handle_instr_cmp,
		'cmpg-float'                : handle_instr_cmp,
		'cmpl-double'               : handle_instr_cmp,
		'cmpg-double'               : handle_instr_cmp,
		'cmp-long'                  : handle_instr_cmp,
		'if-eq'                     : handle_instr_if,
		'if-ne'                     : handle_instr_if,
		'if-lt'                     : handle_instr_if,
		'if-ge'                     : handle_instr_if,
		'if-gt'                     : handle_instr_if,
		'if-le'                     : handle_instr_if,
		'if-eqz'                    : handle_instr_ifz,
		'if-nez'                    : handle_instr_ifz,
		'if-ltz'                    : handle_instr_ifz,
		'if-gez'                    : handle_instr_ifz,
		'if-gtz'                    : handle_instr_ifz,
		'if-lez'                    : handle_instr_ifz,
		'aget'                      : handle_instr_aget,
		'aget-wide'                 : handle_instr_aget,
		'aget-object'               : handle_instr_aget,
		'aget-boolean'              : handle_instr_aget,
		'aget-byte'                 : handle_instr_aget,
		'aget-char'                 : handle_instr_aget,
		'aget-short'                : handle_instr_aget,
		'aput'                      : handle_instr_aput,
		'aput-wide'                 : handle_instr_aput,
		'aput-object'               : handle_instr_aput,
		'aput-boolean'              : handle_instr_aput,
		'aput-byte'                 : handle_instr_aput,
		'aput-char'                 : handle_instr_aput,
		'aput-short'                : handle_instr_aput,
		'iget'                      : handle_instr_iget,
		'iget-wide'                 : handle_instr_iget,
		'iget-object'               : handle_instr_iget,
		'iget-boolean'              : handle_instr_iget,
		'iget-byte'                 : handle_instr_iget,
		'iget-char'                 : handle_instr_iget,
		'iget-short'                : handle_instr_iget,
		'iput'                      : handle_instr_iput,
		'iput-wide'                 : handle_instr_iput,
		'iput-object'               : handle_instr_iput,
		'iput-boolean'              : handle_instr_iput,
		'iput-byte'                 : handle_instr_iput,
		'iput-char'                 : handle_instr_iput,
		'iput-short'                : handle_instr_iput,
		'sget'                      : handle_instr_sget,
		'sget-wide'                 : handle_instr_sget,
		'sget-object'               : handle_instr_sget,
		'sget-boolean'              : handle_instr_sget,
		'sget-byte'                 : handle_instr_sget,
		'sget-char'                 : handle_instr_sget,
		'sget-short'                : handle_instr_sget,
		'sput'                      : handle_instr_sput,
		'sput-wide'                 : handle_instr_sput,
		'sput-object'               : handle_instr_sput,
		'sput-boolean'              : handle_instr_sput,
		'sput-byte'                 : handle_instr_sput,
		'sput-char'                 : handle_instr_sput,
		'sput-short'                : handle_instr_sput,
		'invoke-virtual'            : handle_instr_invoke,
		'invoke-super'              : handle_instr_invoke,
		'invoke-direct'             : handle_instr_invoke,
		'invoke-static'             : handle_instr_invoke,
		'invoke-interface'          : handle_instr_invoke,
		'invoke-virtual/range'      : handle_instr_invoke,
		'invoke-super/range'        : handle_instr_invoke,
		'invoke-direct/range'       : handle_instr_invoke,
		'invoke-static/range'       : handle_instr_invoke,
		'invoke-interface/range'    : handle_instr_invoke,
		'neg-int'                   : handle_instr_unop,
		'not-int'                   : handle_instr_unop,
		'neg-long'                  : handle_instr_unop,
		'not-long'                  : handle_instr_unop,
		'neg-float'                 : handle_instr_unop,
		'neg-double'                : handle_instr_unop,
		'int-to-long'               : handle_instr_unop,
		'int-to-float'              : handle_instr_unop,
		'int-to-double'             : handle_instr_unop,
		'long-to-int'               : handle_instr_unop,
		'long-to-float'             : handle_instr_unop,
		'long-to-double'            : handle_instr_unop,
		'float-to-int'              : handle_instr_unop,
		'float-to-long'             : handle_instr_unop,
		'float-to-double'           : handle_instr_unop,
		'double-to-int'             : handle_instr_unop,
		'double-to-long'            : handle_instr_unop,
		'double-to-float'           : handle_instr_unop,
		'int-to-byte'               : handle_instr_unop,
		'int-to-char'               : handle_instr_unop,
		'int-to-short'              : handle_instr_unop,
		'add-int'                   : handle_instr_binop,
		'sub-int'                   : handle_instr_binop,
		'mul-int'                   : handle_instr_binop,
		'div-int'                   : handle_instr_binop,
		'rem-int'                   : handle_instr_binop,
		'and-int'                   : handle_instr_binop,
		'or-int'                    : handle_instr_binop,
		'xor-int'                   : handle_instr_binop,
		'shl-int'                   : handle_instr_binop,
		'shr-int'                   : handle_instr_binop,
		'ushr-int'                  : handle_instr_binop,
		'add-long'                  : handle_instr_binop,
		'sub-long'                  : handle_instr_binop,
		'mul-long'                  : handle_instr_binop,
		'div-long'                  : handle_instr_binop,
		'rem-long'                  : handle_instr_binop,
		'and-long'                  : handle_instr_binop,
		'or-long'                   : handle_instr_binop,
		'xor-long'                  : handle_instr_binop,
		'shl-long'                  : handle_instr_binop,
		'shr-long'                  : handle_instr_binop,
		'ushr-long'                 : handle_instr_binop,
		'add-float'                 : handle_instr_binop,
		'sub-float'                 : handle_instr_binop,
		'mul-float'                 : handle_instr_binop,
		'div-float'                 : handle_instr_binop,
		'rem-float'                 : handle_instr_binop,
		'add-double'                : handle_instr_binop,
		'sub-double'                : handle_instr_binop,
		'mul-double'                : handle_instr_binop,
		'div-double'                : handle_instr_binop,
		'rem-double'                : handle_instr_binop,
		'add-int/2addr'             : handle_instr_binop_2addr,
		'sub-int/2addr'             : handle_instr_binop_2addr,
		'mul-int/2addr'             : handle_instr_binop_2addr,
		'div-int/2addr'             : handle_instr_binop_2addr,
		'rem-int/2addr'             : handle_instr_binop_2addr,
		'and-int/2addr'             : handle_instr_binop_2addr,
		'or-int/2addr'              : handle_instr_binop_2addr,
		'xor-int/2addr'             : handle_instr_binop_2addr,
		'shl-int/2addr'             : handle_instr_binop_2addr,
		'shr-int/2addr'             : handle_instr_binop_2addr,
		'ushr-int/2addr'            : handle_instr_binop_2addr,
		'add-long/2addr'            : handle_instr_binop_2addr,
		'sub-long/2addr'            : handle_instr_binop_2addr,
		'mul-long/2addr'            : handle_instr_binop_2addr,
		'div-long/2addr'            : handle_instr_binop_2addr,
		'rem-long/2addr'            : handle_instr_binop_2addr,
		'and-long/2addr'            : handle_instr_binop_2addr,
		'or-long/2addr'             : handle_instr_binop_2addr,
		'xor-long/2addr'            : handle_instr_binop_2addr,
		'shl-long/2addr'            : handle_instr_binop_2addr,
		'shr-long/2addr'            : handle_instr_binop_2addr,
		'ushr-long/2addr'           : handle_instr_binop_2addr,
		'add-float/2addr'           : handle_instr_binop_2addr,
		'sub-float/2addr'           : handle_instr_binop_2addr,
		'mul-float/2addr'           : handle_instr_binop_2addr,
		'div-float/2addr'           : handle_instr_binop_2addr,
		'rem-float/2addr'           : handle_instr_binop_2addr,
		'add-double/2addr'          : handle_instr_binop_2addr,
		'sub-double/2addr'          : handle_instr_binop_2addr,
		'mul-double/2addr'          : handle_instr_binop_2addr,
		'div-double/2addr'          : handle_instr_binop_2addr,
		'rem-double/2addr'          : handle_instr_binop_2addr,
		'add-int/lit16'             : handle_instr_binop_lit,
		'rsub-int'                  : handle_instr_binop_lit,
		'mul-int/lit16'             : handle_instr_binop_lit,
		'div-int/lit16'             : handle_instr_binop_lit,
		'rem-int/lit16'             : handle_instr_binop_lit,
		'and-int/lit16'             : handle_instr_binop_lit,
		'or-int/lit16'              : handle_instr_binop_lit,
		'xor-int/lit16'             : handle_instr_binop_lit,
		'add-int/lit8'              : handle_instr_binop_lit,
		'rsub-int/lit8'             : handle_instr_binop_lit,
		'mul-int/lit8'              : handle_instr_binop_lit,
		'div-int/lit8'              : handle_instr_binop_lit,
		'rem-int/lit8'              : handle_instr_binop_lit,
		'and-int/lit8'              : handle_instr_binop_lit,
		'or-int/lit8'               : handle_instr_binop_lit,
		'xor-int/lit8'              : handle_instr_binop_lit,
		'shl-int/lit8'              : handle_instr_binop_lit,
		'shr-int/lit8'              : handle_instr_binop_lit,
		'ushr-int/lit8'             : handle_instr_binop_lit,
		}


generators = {
		'label'                     : ignore,
		'nop'                       : generate_instr_nop,
		'move'                      : generate_instr_move,
		'move/from16'               : generate_instr_move,
		'move/16'                   : generate_instr_move,
		'move-wide'                 : generate_instr_move,
		'move-wide/from16'          : generate_instr_move,
		'move-wide/16'              : generate_instr_move,
		'move-object'               : generate_instr_move,
		'move-object/from16'        : generate_instr_move,
		'move-object/16'            : generate_instr_move,
		'move-result'               : generate_instr_move_result,
		'move-exception'            : generate_instr_move_result,
		'return-void'               : ignore,
		'return'                    : generate_instr_return,
		'const'                     : generate_instr_const,
		'const-high'                : generate_instr_const_high,
		'const-string'              : generate_instr_const_string,
		'const-string/jumbo'        : generate_instr_const_string,
		'const-class'               : generate_instr_const_class,
		'monitor-enter'             : generate_instr_monitor,
		'monitor-exit'              : generate_instr_monitor,
		'check-cast'                : generate_instr_check_cast,
		'instance-of'               : generate_instr_instance_of,
		'array-length'              : generate_instr_array_length,
		'new-instance'              : generate_instr_new_instance,
		'new-array'                 : generate_instr_new_array,
		'filled-new-array'          : generate_instr_filled_new_array,
		'fill-array-data'           : generate_instr_fill_array_data,
		'throw'                     : generate_instr_throw,
		'goto'                      : generate_instr_goto,
		'packed-switch'             : generate_instr_switch,
		'sparse-switch'             : generate_instr_switch,
		'cmp'                       : generate_instr_cmp,
		'if'                        : generate_instr_if,
		'ifz'                       : generate_instr_if,
		'aget'                      : generate_instr_aget,
		'aput'                      : generate_instr_aput,
		'iget'                      : generate_instr_iget,
		'iput'                      : generate_instr_iput,
		'sget'                      : generate_instr_sget,
		'sput'                      : generate_instr_sput,
		'invoke-virtual'            : generate_instr_invoke_virtual,
		'invoke-super'              : generate_instr_invoke_super,
		'invoke-direct'             : generate_instr_invoke_direct,
		'invoke-static'             : generate_instr_invoke_static,
		'invoke-interface'          : generate_instr_invoke_interface,
		'unop'                      : generate_instr_unop,
		'binop'                     : generate_instr_binop,
		'binop/2addr'               : generate_instr_binop_2addr,
		'binop/lit'                 : generate_instr_binop_lit,
		}
