#!/usr/bin/env python
# coding: UTF-8

import sys
import os
import fnmatch
import re
from sets import Set
from lists import *
from cStringIO import StringIO
from collections import deque

ignore = lambda *a: ''

ANALYZE_REFLECTION = True
RUNTIME_EXCEPTIONS = False
HATR_SPLIT = True
EXPLODE_TRANSFERS = True

class Method(object):
	def __init__(self, identifier, clazz, method_name, arg_types, return_type, kind, num_locals):
		self.identifier = identifier
		self.clazz = clazz
		self.method_name = method_name
		self.arg_types = arg_types
		self.return_type = return_type
		self.kind = kind
		self.num_locals = num_locals
		self.handlers = []
		self.catchall_handlers = []
		self.switches = []
		self.array_data = {}
		self.num_instructions = 0
		self.num_regs_total = 0
	
	def __str__(self):
		return str(vars(self))

	def get_tuple(self):
		return (
				self.identifier, 
				self.clazz, 
				self.method_name, 
				self.arg_types, 
				self.return_type,
				self.kind,
				self.num_locals,
				)


class Handler(object):
	def __init__(self, catch_type, label_try_start, label_try_end, label_exception_handler):
		self.catch_type = catch_type
		self.label_try_start = label_try_start
		self.label_try_end = label_try_end
		self.label_exception_handler = label_exception_handler

class Switch(object):
	def __init__(self, label):
		self.label = label
		self.mapping = []  # (switch_case, dest_label) tuples

class MethodSignature(object):
	def __init__(self, identifier, clazz, method_name, arg_types, return_type):
		self.identifier = identifier
		self.clazz = clazz
		self.method_name = method_name
		self.arg_types = arg_types
		self.return_type = return_type
	
	def __str__(self):
		return str(vars(self))

	def get_tuple(self):
		return (
				self.identifier, 
				self.clazz, 
				self.method_name, 
				self.arg_types, 
				self.return_type,
				)


class Address(object):
	def __init__(self, method, pc):
		self.method = method
		self.pc = pc
	
	def next(self):
		return Address(self.method, self.pc + 1)

	def __str__(self):
		return '%d @ %s' % (self.pc, self.method.identifier)


class InstructionList(object):
	def __init__(self):
		self._list = []  # tuples of address, line, instruction name, instruction data
	
	def append_instruction(self, line, name, data):
		self._list.append((Globals.current_address, line, name, data))
		if name != 'label':
			Globals.current_address = Globals.current_address.next()
			Globals.current_address.method.num_instructions += 1
	
	def __str__(self):
		return '\n'.join([str((str(x[0]), x[2], x[3])) for x in self._list])


class Globals(object):
	__shared_state = {}
	def __init__(self):
		self.__dict__ = self.__shared_state

	classes = []  # class names
	interfaces = []  # interface names
	parents = []  # (parent, child) tuples
	implements = []  # (class, interface) tuples
	methods = []  # Method objects
	method_signatures = []  # MethodSignature objects
	labels = []  # (label_name, address) tuples 
	instructions = InstructionList()

	num_backedges = 0

	current_class = ''  # class or interface
	current_address = Address(None, 0)

	in_annotation = False
	in_switch = None
	in_array_data = False

	generate_warnings = True

	resolve_fact_map = {}
	hatR_map = {}


# print 'dir2', dir()

from instructions import *



### Helpers

def parse_args(args_raw):
	args = []
	while args_raw != '':
		arg, rest = parse_arg(args_raw)
		args.append(arg)
		args_raw = rest
	return args

def parse_arg(arg_raw):
	first_char, rest_raw = arg_raw[0], arg_raw[1:]
	if first_char in ['V', 'Z', 'B', 'S', 'C', 'I', 'J', 'F', 'D']:
		return first_char, rest_raw
	if first_char == 'L':
		fqn, rest_raw = rest_raw.split(';', 1)
		return 'L%s;' % fqn, rest_raw
	if first_char == '[':
		arg, rest_raw = parse_arg(rest_raw)
		return '[' + arg, rest_raw

def label_lookup(label, method):
	tups = [tup for tup in Globals.labels if tup[0] == label and tup[1].method.identifier == method.identifier]
	if len(tups) != 1:
		raise ValueError('Ambiguous label lookup: %s' % tups)
	return tups[0][1].pc


### Handlers

def handle_class(line, first_word, rest):
	parts = rest.split()
	modifiers, class_name = parts[:-1], parts[-1]
	Globals.current_class = class_name

	if 'interface' in modifiers:
		Globals.interfaces.append(class_name)
	else:
		Globals.classes.append(class_name)


def handle_super(line, first_word, rest):
	Globals.parents.append((rest, Globals.current_class))


def handle_implements(line, first_word, rest):
	Globals.implements.append((rest, Globals.current_class))


def handle_method(line, first_word, rest):
	parts = rest.split()
	modifiers, identifier = parts[:-1], parts[-1]

	if 'constructor' in modifiers or 'private' in modifiers or 'final' in modifiers:
		kind = 'direct'
	elif 'static' in modifiers:
		kind = 'static'  # static constructors have already been matched
	else:
		kind = 'virtual'

	method_name, arg_types_raw, return_type_raw = re.match('([^(]*)\(([^)]*)\)(.*)', identifier).groups()
	arg_types = parse_args(arg_types_raw)
	return_type, should_be_empty = parse_arg(return_type_raw)
	if should_be_empty != '':
		raise ValueError('The return type "%s" was not recognized.' % return_type_raw)

	if 'abstract' in modifiers or 'native' in modifiers:
		# method_signature = MethodSignature(Globals.current_class + '->' + identifier, Globals.current_class, method_name, arg_types, return_type)
		# Globals.method_signatures.append(method_signature)
		pass
	else:
		method = Method(Globals.current_class + '->' + identifier, Globals.current_class, method_name, arg_types, return_type, kind, 'UNKNOWN')
		Globals.current_address = Address(method, 0)
		Globals.methods.append(method)


def handle_locals(line, first_word, rest):
	method = Globals.methods[-1]
	method.num_locals = int(rest)
	method.num_regs_total = method.num_locals + len(method.arg_types) + len([t for t in method.arg_types if t in ('J', 'D')]) + int(method.kind != 'static')


def handle_label(line, first_word, rest):
	if rest != '':
		raise ValueError('Label followed by garbage: %s' % rest)
	Globals.labels.append((first_word, Globals.current_address))
	Globals.instructions.append_instruction(first_word, 'label', [])


def handle_catch(line, first_word, rest):
	catch_type, label_try_start, label_try_end, label_exception_handler = re.match('([^ ]*) {(:[^ ]*) .. (:[^}]*)} (:.*)', rest).groups()
	Globals.current_address.method.handlers.append(Handler(catch_type, label_try_start, label_try_end, label_exception_handler))


def handle_catchall(line, first_word, rest):
	label_try_start, label_try_end, label_exception_handler = re.match('{(:[^ ]*) .. (:[^}]*)} (:.*)', rest).groups()
	Globals.current_address.method.catchall_handlers.append(Handler('top_exception', label_try_start, label_try_end, label_exception_handler))


def handle_switch_data(line, first_word, rest):
	if not Globals.in_switch:
		Globals.in_switch = first_word
		Globals.current_address.method.switches.append(Switch(Globals.labels[-1]))
		if first_word == '.packed-switch':
			Globals.current_address.method.switches[-1].mapping.append((rest, 'UNKNOWN'))
	elif first_word == '.end' and rest in ('sparse-switch', 'packed-switch'):
		Globals.in_switch = None
	else:
		if Globals.in_switch == '.sparse-switch':
			case = first_word
			_, dest = rest.split(' ')
		else:
			if Globals.current_address.method.switches[-1].mapping[0][1] == 'UNKNOWN':
				case = Globals.current_address.method.switches[-1].mapping[-1][0]
				Globals.current_address.method.switches[-1].mapping = []
			else:
				case = hex(int(Globals.current_address.method.switches[-1].mapping[-1][0], 16) + 1)
			dest = first_word
		Globals.current_address.method.switches[-1].mapping.append((case, dest))


def handle_array_data(line, first_word, rest):
	current_label = Globals.labels[-1][0]
	if not Globals.in_array_data:
		Globals.in_array_data = True
		Globals.current_address.method.array_data.update({current_label: []})
	elif first_word == '.end' and rest == 'array-data':
		Globals.in_array_data = False
	else:
		s = first_word + ' ' + rest
		l = ['%02x' % int(b[2:-1], 16) for b in s.split()]
		l.reverse()  # reverse big endian
		value = '0x' + ''.join(l)
		Globals.current_address.method.array_data[current_label].append(value)


def parse_line(line_number, line):
	'''Nice stateful function'''
	line = line.strip()
	if line == '':
		return

	split_line = line.split(None, 1)
	if len(split_line) == 1:
		first_word, rest = line, ''
	else:
		first_word, rest = split_line

	# print line
	
	if not Globals.in_annotation:
		if first_word == '.annotation':
			Globals.in_annotation = True
			return
	else:
		if line == '.end annotation':
			Globals.in_annotation = False
		return
	
	if Globals.in_switch:
		handle_switch_data(line, first_word, rest)
		return

	elif Globals.in_array_data:
		handle_array_data(line, first_word, rest)
		return

	func_dict = {
			'.line': ignore,

			'.end': ignore,

			'.class': handle_class,
			'.super': handle_super,
			'.source': ignore,
			'.implements': handle_implements,
			'.field': ignore,  # implement if necessary

			'.method': handle_method,
			'.locals': handle_locals,
			'.parameter': ignore,
			'.prologue': ignore,
			'.local': ignore,
			'.restart': ignore,

			'.catch': handle_catch,
			'.catchall': handle_catchall,

			'.sparse-switch': handle_switch_data,
			'.packed-switch': handle_switch_data,

			'.array-data': handle_array_data,
			}

	func_dict.update(instruction_dict)

	try:
		func_dict[first_word](line, first_word, rest)
	except KeyError:
		if first_word[0] == '#':
			return
		if first_word[0] == ':':
			handle_label(line, first_word, rest)
		else:
			print >> sys.stderr, 'Unhandled case: "%s" at line %d' % (first_word, line_number)


def main():
	if len(sys.argv) == 1:
		print >> sys.stderr, "Usage: %s [app_path]" % sys.argv[0]
		sys.exit(1)

	app_path = sys.argv[1]

	if len(sys.argv) == 3 and sys.argv[2] == '--nowarn':
		Globals.generate_warnings = False

	if not os.path.exists(app_path):
		print >> sys.stderr, "App path not found"
		sys.exit(1)

	# Parse
	for root, dirs, files in os.walk(app_path):
		for filename in fnmatch.filter(files, '*.smali'):
			file_path = os.path.join(root, filename)
			# print >> sys.stderr, 'Analyzing %s ...' % file_path
			with open(file_path) as f:
				for index, line in enumerate(f.readlines()):
					parse_line(index + 1, line)

	# print >> sys.stderr, "Parsing done"
	# os.system('date')


	# Build type hierarchy
	# Tree implementation from http://kmkeen.com/python-trees/2009-05-30-11-05-40-138.html
	tree = {}
	for parent, child in Globals.parents:
		if parent not in tree:
			tree[parent] = []
		if child not in tree:
			tree[child] = []
		tree[parent].append(child)

	def compatible(method1, method2):
		return (method1.method_name == method2.method_name and
				method1.arg_types == method2.arg_types and
				method1.return_type == method2.return_type and
				method1.kind == method2.kind)

	def walk(token, tree, given_method, depth=0):
		'''Return all classes below the given class that do not implement the given method'''
		# print >> sys.stderr, '  ' * depth + token

		clazzes = []
		if depth > 0:
			for method in Globals.methods:
				if method.clazz == token and compatible(method, given_method):
					return []  # No classes down the current path will resolve to given_method, return and don't recurse
			clazzes.append(token)

		for child in tree[token]:
			clazzes += walk(child, tree, given_method, depth + 1)
		return clazzes

	resolve_fact_strings = []

	# For each method, go down the class hierarchy and find all classes that
	# resolve to (i.e., do not have an implementation of) the method
	for method in Globals.methods:
		for clazz in walk(method.clazz, tree, method) + [method.clazz]:
			key = clazz + method.method_name + str(method.arg_types) + method.return_type + method.kind
			Globals.resolve_fact_map[key] = method.identifier
			resolve_fact_strings.append("resolveFact(%s, %s, %s, %s, %s).\n" % (
				quote(clazz),
				quote(method.method_name),
				method.arg_types,
				quote(method.return_type),
				quote(method.identifier),
				))

	# Generate
	output = file("out.pl", "w")

	with open(os.path.join(os.path.dirname(__file__), 'prelude.pl')) as f:
		output.write('\n' + f.read())


	output.write("\n\n\n%%% Resolve facts\n")
	output.writelines(resolve_fact_strings)


	output.write('\n%%% Classes\n')
	clazz_lines = Set()
	for clazz in Globals.classes:
		clazz_lines.add("class('%s').\n" % clazz)
	for parent, child in Globals.parents:
		if parent != 'Ljava/lang/Object;':
			clazz_lines.add("class('%s').\n" % parent)
	for clazz_line in clazz_lines:
		output.write(clazz_line)
	
	# Relation not used:
	# output.write('\n%%% Interfaces\n')
	# for interface in Globals.interfaces:
	# 	output.write("interface('%s').\n" % interface)
	
	output.write('\n%%% Super relationships\n')
	for parent in Globals.parents:
		output.write("super('%s', '%s').\n" % parent)

	# Relation not used:
	# output.write('\n%%% Implements relationships\n')
	# for implements in Globals.implements:
	# 	output.write("implements('%s', '%s').\n" % implements)

	# Relation not used:
	# output.write('\n%%% Method signatures\n')
	# signature_lines = Set()
	# for method_signature in Globals.method_signatures:
	# 	signature_lines.add("methodsignature('%s', '%s', '%s', %s, '%s').\n" % method_signature.get_tuple())
	# for signature_line in signature_lines:
	# 	output.write(signature_line)

	output.write('\n%%% Methods\n')
	for method in Globals.methods:
		output.write("method('%s', '%s', '%s', %s, '%s', %s, %d).\n" % method.get_tuple())
		if HATR_SPLIT:
			output.write(
				clause(
					"hatR('%s', PC, V, Y)" % method.identifier,
					hatRraw(quote(method.identifier), 'PC', 'V', 'Y')
				))
		for index, handler in enumerate(method.handlers):
			output.write("\thandler('%s', %d, '%s', %d, %d, %d).\n" % (
					method.identifier,
					index,
					handler.catch_type,
					label_lookup(handler.label_try_start, method),
					label_lookup(handler.label_try_end, method),
					label_lookup(handler.label_exception_handler, method),
					))
		for index, handler in enumerate(method.catchall_handlers):
			output.write("\thandler('%s', %d, '%s', %d, %d, %d).\n" % (
					method.identifier,
					index + len(method.handlers),
					handler.catch_type,
					label_lookup(handler.label_try_start, method),
					label_lookup(handler.label_try_end, method),
					label_lookup(handler.label_exception_handler, method),
					))

	
	def superclasses(clazz):
		for parent, child in Globals.parents:
			if clazz == child:
				return [clazz] + superclasses(parent)
		return [clazz]
	

	# print >> sys.stderr, "Facts done, generating entry points"
	# os.system('date')

	output.write('\n%%% Entry Points\n')

	for clazz in Globals.classes:
		clazz = clazz.replace('$', '/')  ######## 
		for entry_clazz in android_class_methods_map:
			if entry_clazz in superclasses(clazz):
				for method in Globals.methods:
					if method.clazz == clazz:
						_, half_sig = method.identifier.split('->')
						if half_sig in android_class_methods_map[entry_clazz]:
							output.write(clause(hatRraw(
								quote(method.identifier),
								0,
								method.num_locals,
								"(%s, _, _)" % quote(clazz),  # It doesn't matter who created the object the entry point method is being invoked on
								)))  # No entry point class methods are static
							for index, arg_type in enumerate(method.arg_types):
								output.write(clause(hatRraw(
									quote(method.identifier),
									0,
									method.num_locals + 1 + index,
									("(%s, android, 0)" % quote(arg_type)) if is_reference_type(arg_type) else 'top_prim',
									)))

	for interface, clazz in Globals.implements:
		interface = interface.replace('$', '/')  ######## 
		if interface in android_interface_methods_map:
			for method in Globals.methods:
				if method.clazz == clazz:
					_, half_sig = method.identifier.split('->')
					if half_sig in android_interface_methods_map[interface]:
						output.write(clause(hatRraw(
							quote(method.identifier),
							0,
							method.num_locals,
							"(%s, _, _)" % quote(clazz),  # It doesn't matter who created the object the entry point method is being invoked on
							)))  # No interface methods are static
						for index, arg_type in enumerate(method.arg_types):
							output.write(clause(hatRraw(
								quote(method.identifier),
								0,
								method.num_locals + 1 + index,
								("(%s, android, 0)" % quote(arg_type)) if is_reference_type(arg_type) else 'top_prim',
								)))

	# print >> sys.stderr, "Entry points done, generating instructions"
	# os.system('date')

	output.write('\n\n\n%%% Instructions\n')
	current_method_identifier = ''
	for address, smali_line, name, data in Globals.instructions._list:
		if current_method_identifier != address.method.identifier:
			current_method_identifier = address.method.identifier
			output.write('\n\n\n%%%%%% Method %s\n' % current_method_identifier)

			reg_numbers = range(address.method.num_locals, address.method.num_regs_total)
			output.write(''.join(clause(
				hatRraw(quote(current_method_identifier), 0, reg_number, 'Y'),
				"invoke(%s, %d, Y)" % (quote(current_method_identifier), reg_number - address.method.num_locals)
				) for reg_number in reg_numbers)
				)

			# for i in range(address.method.num_locals):
			# 	output.write(clause(hatR(address, i, "'bot_init_%s'" % (i), "_")))
			# output.write(clause(hatR(address, "retval", "bot_retval_init", "_")))
		output.write('\n%% %d: %s\n' % (address.pc, smali_line))
		try:
			output.write(generators[name](address, data))
		except KeyError:
			print >> sys.stderr, 'Unhandled instr: "%s"' % name

	# print >> sys.stderr, "Instructions done"
	# os.system('date')


	output.write('\n\n\n%%% Smali lines\n')
	current_method_identifier = ''
	for address, smali_line, name, data in Globals.instructions._list:
		if current_method_identifier != address.method.identifier:
			current_method_identifier = address.method.identifier
			label = ''
		if smali_line.startswith(':'):
			label = smali_line
			continue
		smali_line = smali_line.replace("\\'", "BACKSLASH-SINGLEQ")
		output.write("smaliLine(%s, %d, %s).\n" % (
			quote(address.method.identifier),
			address.pc,
			quote(((label + '\\n') if label else '') + smali_line),
			))
		label = ''


	output.write('\n\n\n%%% Method lengths\n')
	for method in Globals.methods:
		output.write("methodLength(%s, %d).\n" % (
			quote(method.identifier),
			method.num_instructions,
			))

	output.write('\n\n\n')

	with open(os.path.join(os.path.dirname(__file__), 'postlude.pl')) as f:
		output.write('\n' + f.read())

	# print output.getvalue()
	output.close()


if __name__ == '__main__':
	main()
	sys.stdout.flush()


