% flow logic tests


%%% Generated

% For each ".class" without "interface". Should include full package path
class('Lcom/test/MyClass;').
class('Lcom/test/MySubClass;').
class('Lcom/test/MySubSubClass;').

% For each ".class" with "interface"
interface(iface_MyInterface).

% For each ".super" on a ".class"
parent('Lcom/test/MyClass;', 'Lcom/test/MySubClass;').
parent('Lcom/test/MySubClass;', 'Lcom/test/MySubSubClass;').

% ".implements [i]" in class [c]
implements('Lcom/test/MyClass;', iface_MyInterface).

% each invoke (collect them during parsing and group them in the output)
methodsignature('Lcom/test/MySubClass;->foo(I)I', 'Lcom/test/MySubClass;', foo, [int], int).
methodsignature('Lcom/test/MySubSubClass;->foo(I)I', 'Lcom/test/MySubSubClass;', foo, [int], int).
methodsignature('Lcom/test/MySubSubClass;->foo(II)I', 'Lcom/test/MySubSubClass;', foo, [int, int], int).  % overloaded

methodsignature('Lcom/test/MyClass;-><init>(I)I', 'Lcom/test/MyClass;', '<init>', [int], int).
methodsignature('Lcom/test/MySubSubClass;-><init>(I)I', 'Lcom/test/MySubSubClass;', '<init>', [int], int).
methodsignature('Lcom/test/MySubSubClass;->staticFoo(II)I', 'Lcom/test/MySubSubClass;', staticFoo, [int, int], int).

% ".method"
method('Lcom/test/Kl;->start()V', 'Lcom/test/Kl;', start, [], V, virtual, 5).
method('Lcom/test/Kl;->startTwo()V', 'Lcom/test/Kl;', start, [], V, virtual, 2).
method('Lcom/test/MyClass;->foo(I)I', 'Lcom/test/MyClass;', foo, [int], int, virtual, 2).
method('Lcom/test/MySubSubClass;->foo(I)I', 'Lcom/test/MySubSubClass;', foo, [int], int, virtual, 2).

method('Lcom/test/MyClass;-><init>(I)I', 'Lcom/test/MyClass;', '<init>', [int], int, direct, 2).
method('Lcom/test/MyClass;->staticFoo(II)I', 'Lcom/test/MyClass;', 'staticFoo', [int, int], int, static, 2).









%%% An example program

% pc=0 nop                      pc v  content
hatR('Lcom/test/Kl;->start()V', 1, V, Y) :-
	hatR('Lcom/test/Kl;->start()V', 0, V, Y).

% pc=1 const v0 11
hatR('Lcom/test/Kl;->start()V', 2, 0, 11).
hatR('Lcom/test/Kl;->start()V', 2, V, Y) :-
	hatR('Lcom/test/Kl;->start()V', 1, V, Y),
	V \= 0.

% pc=2 const v1 22
hatR('Lcom/test/Kl;->start()V', 3, 1, 22 << 2).
hatR('Lcom/test/Kl;->start()V', 3, V, Y) :-
	hatR('Lcom/test/Kl;->start()V', 2, V, Y),
	V \= 1.

% pc=3 add-int v0 v0 v1
hatR('Lcom/test/Kl;->start()V', 4, 0, Z) :-
	hatR('Lcom/test/Kl;->start()V', 3, 0, A),
	hatR('Lcom/test/Kl;->start()V', 3, 1, B),
	binop(plus, A, B, Z).
hatR('Lcom/test/Kl;->start()V', 4, V, Y) :-
	hatR('Lcom/test/Kl;->start()V', 3, V, Y),
	V \= 0.

% pc=4 if eq v0 v1 7
hatR('Lcom/test/Kl;->start()V', 7, V, Y) :- % true
	hatR('Lcom/test/Kl;->start()V', 4, V, Y).
hatR('Lcom/test/Kl;->start()V', 5, V, Y) :- % false
	hatR('Lcom/test/Kl;->start()V', 4, V, Y).

% pc=5 move v2 v0
hatR('Lcom/test/Kl;->start()V', 6, 2, Y) :-
	hatR('Lcom/test/Kl;->start()V', 5, 0, Y).
hatR('Lcom/test/Kl;->start()V', 6, V, Y) :-
	hatR('Lcom/test/Kl;->start()V', 5, V, Y),
	V \= 2.

% pc=6 new-instance v0 MySubClass
hatR('Lcom/test/Kl;->start()V', 7, 0, 'Lcom/test/MySubClass;').
hatR('Lcom/test/Kl;->start()V', 7, V, Y) :-
	hatR('Lcom/test/Kl;->start()V', 6, V, Y),
	V \= 0.

% pc=7 instance-of v2 v0 MyClass
hatR('Lcom/test/Kl;->start()V', 8, 2, 0).
hatR('Lcom/test/Kl;->start()V', 8, 2, 1).
hatR('Lcom/test/Kl;->start()V', 8, V, Y) :-
	hatR('Lcom/test/Kl;->start()V', 7, V, Y),
	V \= 2.

% pc=8 iput v2 v0 Lcom/test/MyClass;->value
hatH(CL, value, Y) :-
	hatR('Lcom/test/Kl;->start()V', 8, 0, CL),
	objref(CL),
	hatR('Lcom/test/Kl;->start()V', 8, 2, Y).
hatR('Lcom/test/Kl;->start()V', 9, V, Y) :-
	hatR('Lcom/test/Kl;->start()V', 8, V, Y).

% pc=9 iget v3 v0 value
hatR('Lcom/test/Kl;->start()V', 10, 3, Y) :-
	hatR('Lcom/test/Kl;->start()V', 9, 0, CL),
	objref(CL),
	hatH(CL, value, Y).
hatR('Lcom/test/Kl;->start()V', 10, V, Y) :-
	hatR('Lcom/test/Kl;->start()V', 9, V, Y),
	V \= 3.

% pc=10 invoke-virtual v0 v3 'Lcom/test/MySubClass;->foo(I)I'
hatR(FullName, 0, V, Y) :-
	FullName \= 'Lcom/test/Kl;->start()V',  % this does not hold for immediate recursion
	objref(CL),  % TODO: Add runtime exception for nulls
	MethodSignature = methodsignature('Lcom/test/MySubClass;->foo(I)I', _, _, _, _),
	resolveMethod(virtual, MethodSignature, CL, Method),
	Method = method(FullName, _, _, _, _, _, MaxLocal),
	hatR('Lcom/test/Kl;->start()V', 10, 0, CL),
	(
		( V is MaxLocal + 1, hatR('Lcom/test/Kl;->start()V', 10, 0, Y), objref(Y) ) ;
		( V is MaxLocal + 2, hatR('Lcom/test/Kl;->start()V', 10, 3, Y) )   % TODO: filter values by type from signature
	).
hatR('Lcom/test/Kl;->start()V', 11, retval, Y) :-
	hatR('Lcom/test/Kl;->start()V', 10, 0, CL),
	objref(CL),
	MethodSignature = methodsignature('Lcom/test/MySubClass;->foo(I)I', _, _, _, _),
	resolveMethod(virtual, MethodSignature, CL, Method),
	% Method \= bot,        % A runtime exception should make the program fail.
	Method = method(FullName, _, _, _, ReturnType, _, _),
	ReturnType \= void,
	hatR(FullName, end, end, Y).
hatR('Lcom/test/Kl;->start()V', 11, V, Y) :-
	hatR('Lcom/test/Kl;->start()V', 10, V, Y),
	V \= retval.

% pc=11 move-result v4
hatR('Lcom/test/Kl;->start()V', 12, 4, Y) :-
	hatR('Lcom/test/Kl;->start()V', 11, retval, Y).
hatR('Lcom/test/Kl;->start()V', 12, V, Y) :-
	hatR('Lcom/test/Kl;->start()V', 11, V, Y),
	V \= 4.









%%%%%%%%%%%%%%%%%%%%%%% startTwo

% pc=0 new-instance v0 MySubClass
hatR('Lcom/test/Kl;->startTwo()V', 1, 0, 'Lcom/test/MySubClass;').
hatR('Lcom/test/Kl;->startTwo()V', 1, V, Y) :-
	hatR('Lcom/test/Kl;->startTwo()V', 0, V, Y),
	V \= 0.

% pc=1 const v1 15
hatR('Lcom/test/Kl;->startTwo()V', 2, 1, 15).
hatR('Lcom/test/Kl;->startTwo()V', 2, V, Y) :-
	hatR('Lcom/test/Kl;->startTwo()V', 1, V, Y),
	V \= 1.

% pc=2 invoke-virtual v0 v1 'Lcom/test/MySubClass;->foo(I)I'
hatR(FullName, 0, V, Y) :-
	FullName \= 'Lcom/test/Kl;->startTwo()V',  % this does not hold for immediate recursion
	objref(CL),  % TODO: Add runtime exception for nulls
	MethodSignature = methodsignature('Lcom/test/MySubClass;->foo(I)I', _, _, _, _),
	resolveMethod(virtual, MethodSignature, CL, Method),
	Method = method(FullName, _, _, _, _, _, MaxLocal),
	hatR('Lcom/test/Kl;->startTwo()V', 2, 0, CL),
	(
		( V is MaxLocal + 1, hatR('Lcom/test/Kl;->startTwo()V', 2, 0, Y), objref(Y) ) ;
		( V is MaxLocal + 2, hatR('Lcom/test/Kl;->startTwo()V', 2, 1, Y) )   % TODO: filter values by type from signature
	).
hatR('Lcom/test/Kl;->startTwo()V', 3, retval, Y) :-
	hatR('Lcom/test/Kl;->startTwo()V', 2, 0, CL),
	objref(CL),
	MethodSignature = methodsignature('Lcom/test/MySubClass;->foo(I)I', _, _, _, _),
	resolveMethod(virtual, MethodSignature, CL, Method),
	% Method \= bot,        % A runtime exception should make the program fail.
	Method = method(FullName, _, _, _, ReturnType, _, _),
	ReturnType \= void,
	hatR(FullName, end, end, Y).
hatR('Lcom/test/Kl;->startTwo()V', 3, V, Y) :-
	hatR('Lcom/test/Kl;->startTwo()V', 2, V, Y),
	V \= retval.










% class MyClass { int foo(int lol) { return lol; } }
% pc=0 return p1
hatR('Lcom/test/MyClass;->foo(I)I', end, end, Y) :-
	hatR('Lcom/test/MyClass;->foo(I)I', 0, 4, Y).

% class MyClass { int staticFoo(int lol, int lol2) { return lol2; } }
% pc=0 return p1
hatR('Lcom/test/MyClass;->staticFoo(II)I', end, end, Y) :-
	% hatR('Lcom/test/MyClass;->staticFoo(II)I', 0, 4, Y).
	Y = 44.



% nop
% const
% move
% if (uden back-edges)
%
% new-instance
% instance-of
% iget
% iput
%
% invoke-virtual (uden rekursion)
% return
% move-result




%%% Standard stuff
parent(bot, 'Ljava/lang/Object;').
class('Ljava/lang/Object;').
kind(virtual). kind(static). kind(direct).
binop(plus, A, B, Z) :-
	Z is A + B.

% The conditions for super are carefully ordered such that Prolog resolves from the bottom of class hierarchy and up
super(A, B) :-
	parent(A, B),
	class(A).
super(A, B) :-
	parent(X, B),
	super(A, X).

methodsignature(MethodSignature) :-
	MethodSignature = methodsignature(FullName, ClassOrInterface, Name, ArgTypes, ReturnType),
	methodsignature(FullName, ClassOrInterface, Name, ArgTypes, ReturnType).
	% ( class(ClassOrInterface) ; interface(ClassOrInterface) ).  % This should already be checked by the bytecode verifier

method(Method) :-
	Method = method(FullName, Class, Name, ArgTypes, ReturnType, Kind, MaxLocal),
	method(FullName, Class, Name, ArgTypes, ReturnType, Kind, MaxLocal).
	% class(Class),
	% kind(Kind).

compatible(MethodSignature, Method) :-
	methodsignature(MethodSignature),
	method(Method),
	MethodSignature = methodsignature(_, _, Name, ArgTypes, ReturnType),
	Method = method(_, _, Name, ArgTypes, ReturnType, _, _).

resolveMethod(Kind, MethodSignature, CL, Head) :-
	findall(Method, inner_resolveMethod(Kind, MethodSignature, CL, Method), [Head|_]).
inner_resolveMethod(Kind, MethodSignature, CL, Method) :-
	Method = method(_, CL, _, _, _, Kind, _),
	class(CL),
	compatible(MethodSignature, Method).
inner_resolveMethod(Kind, MethodSignature, CL, Method) :-
	Kind \= direct,
	parent(P, CL),
	inner_resolveMethod(Kind, MethodSignature, P, Method).
inner_resolveMethod(_, _, bot, bot).

objref(X) :-
	class(X).  % TODO: Represent null



% Example queries
% resolveMethod(virtual, methodsignature('Lcom/test/MySubClass;->foo(I)I', _, _, _, _), 'Lcom/test/MySubClass;', Method).
% hatR('Lcom/test/Kl;->start()V', 11, V, Y).
% setof(Y, hatR('Ldk/aau/sw9/examples/MiniAppActivity;->onCreate(Landroid/os/Bundle;)V', PC, V, Y), Bag).
% setof(Content, hatH(CL, Field, Content), Set).
