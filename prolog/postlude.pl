%%% Hardcoded app-specific

% method('Landroid/app/Activity;-><init>()V', 'Landroid/app/Activity;', '<init>', [], 'V', direct, 0).
% method('Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V', 'Landroid/app/Activity;', 'onCreate', ['Landroid/os/Bundle;'], 'V', virtual, 0).

class('Landroid/util/Log;').
method('Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I', 'Landroid/util/Log;', 'e', ['Ljava/lang/String;', 'Ljava/lang/String;', 'Ljava/lang/Throwable;'], 'I', static, 0).
% hatR('Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I', end, end, int_log).


class('Landroid/os/Bundle;').
super('Ljava/lang/Object;', 'Landroid/os/Bundle;').

class('Ljava/nio/BufferOverflowException;').
super('Ljava/lang/RuntimeException;', 'Ljava/nio/BufferOverflowException;').


%%% Analysis specific Android stuff
resolveFact('Landroid/telephony/SmsManager;', 'sendTextMessage', ['Ljava/lang/String;', 'Ljava/lang/String;', 'Ljava/lang/String;', 'Landroid/app/PendingIntent;', 'Landroid/app/PendingIntent;'], 'V', 'Landroid/telephony/SmsManager;->sendTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)V').
resolveFact(top_ref,                          'sendTextMessage', ['Ljava/lang/String;', 'Ljava/lang/String;', 'Ljava/lang/String;', 'Landroid/app/PendingIntent;', 'Landroid/app/PendingIntent;'], 'V', 'Landroid/telephony/SmsManager;->sendTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)V').
resolveFact('Landroid/telephony/SmsManager;', 'sendMultipartTextMessage', ['Ljava/lang/String;', 'Ljava/lang/String;', 'Ljava/util/ArrayList;', 'Ljava/util/ArrayList;', 'Ljava/util/ArrayList;'], 'V', 'Landroid/telephony/SmsManager;->sendMultipartTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V').
resolveFact(top_ref                         , 'sendMultipartTextMessage', ['Ljava/lang/String;', 'Ljava/lang/String;', 'Ljava/util/ArrayList;', 'Ljava/util/ArrayList;', 'Ljava/util/ArrayList;'], 'V', 'Landroid/telephony/SmsManager;->sendMultipartTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V').

class('Landroid/telephony/SmsManager;').

method('Landroid/telephony/SmsManager;->sendTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)V', 'Landroid/telephony/SmsManager;', 'sendTextMessage', ['Ljava/lang/String;', 'Ljava/lang/String;', 'Ljava/lang/String;', 'Landroid/app/PendingIntent;', 'Landroid/app/PendingIntent;'], 'V', virtual, 0).
method('Landroid/telephony/SmsManager;->sendMultipartTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V', 'Landroid/telephony/SmsManager;', 'sendMultipartTextMessage', ['Ljava/lang/String;', 'Ljava/lang/String;', 'Ljava/util/ArrayList;', 'Ljava/util/ArrayList;', 'Ljava/util/ArrayList;'], 'V', virtual, 0).


% Specific for webkit/WebView and addJavascriptInterface
resolveFact('Landroid/webkit/WebView;', 'addJavascriptInterface', ['Ljava/lang/Object;', 'Ljava/lang/String;'], 'V', 'Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V').
resolveFact(top_ref, 'addJavascriptInterface', ['Ljava/lang/Object;', 'Ljava/lang/String;'], 'V', 'Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V').
class('Landroid/webkit/WebView').

method('Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V', 'Landroid/webkit/WebView;', 'addJavascriptInterface', ['Ljava/lang/Object;', 'Ljava/lang/String;'], 'V', virtual, 0).

%%% Java Standard Exceptions
super('Ljava/lang/Object;', 'Ljava/lang/Throwable;').
super('Ljava/lang/Throwable;', 'Ljava/lang/Exception;').
super('Ljava/lang/Throwable;', 'Ljava/lang/Error;').
super('Ljava/lang/Exception;', 'Ljava/lang/RuntimeException;').
super('Ljava/lang/Exception;', 'Ljava/lang/ClassNotFoundException;').
super('Ljava/lang/Exception;', 'Ljava/lang/IllegalAccessException;').
super('Ljava/lang/Error;', 'Ljava/lang/LinkageError;').
super('Ljava/lang/LinkageError;', 'Ljava/lang/VerifyError;').
super('Ljava/lang/RuntimeException;', 'Ljava/lang/IllegalMonitorStateException;').
super('Ljava/lang/RuntimeException;', 'Ljava/lang/NegativeArraySizeException;').
super('Ljava/lang/RuntimeException;', 'Ljava/lang/IndexOutOfBoundsException;').
super('Ljava/lang/IndexOutOfBoundsException;', 'Ljava/lang/ArrayIndexOutOfBoundsException;').
super('Ljava/lang/RuntimeException;', 'Ljava/lang/ArithmeticException;').
super('Ljava/lang/RuntimeException;', 'Ljava/lang/ClassCastException;').
super('Ljava/lang/RuntimeException;', 'Ljava/lang/NullPointerException;').

class('Ljava/lang/Throwable;').
class('Ljava/lang/Exception;').
class('Ljava/lang/Error;').
class('Ljava/lang/RuntimeException;').
class('Ljava/lang/ArithmeticException;').
class('Ljava/lang/IndexOutOfBoundsException;').
class('Ljava/lang/ArrayIndexOutOfBoundsException;').
class('Ljava/lang/ClassCastException;').
class('Ljava/lang/ClassNotFoundException;').
class('Ljava/lang/IllegalAccessException;').
class('Ljava/lang/IllegalMonitorStateException;').
class('Ljava/lang/NegativeArraySizeException;').
class('Ljava/lang/NullPointerException;').
class('Ljava/lang/VerifyError;').
class('Ljava/lang/LinkageError;').


%%% Standard Java stuff

class('Ljava/lang/Class;').
class('Ljava/lang/reflect/Method;').

class('Ljava/lang/String;').  % Due const-string
super(bot, 'Ljava/lang/Object;').
class('Ljava/lang/Object;').  % Object must be the last class (?)

hatS('Ljava/lang/Integer;', 'TYPE:Ljava/lang/Class;', ('Ljava/lang/Class;', java, 1)).
hatS('Ljava/lang/Boolean;', 'TYPE:Ljava/lang/Class;', ('Ljava/lang/Class;', java, 2)).
hatS('Ljava/lang/Void;', 'TYPE:Ljava/lang/Class;', ('Ljava/lang/Class;', java, 3)).
hatS('Ljava/lang/Byte;', 'TYPE:Ljava/lang/Class;', ('Ljava/lang/Class;', java, 4)).
hatS('Ljava/lang/Short;', 'TYPE:Ljava/lang/Class;', ('Ljava/lang/Class;', java, 5)).
hatS('Ljava/lang/Character;', 'TYPE:Ljava/lang/Class;', ('Ljava/lang/Class;', java, 6)).
hatS('Ljava/lang/Long;', 'TYPE:Ljava/lang/Class;', ('Ljava/lang/Class;', java, 7)).
hatS('Ljava/lang/Float;', 'TYPE:Ljava/lang/Class;', ('Ljava/lang/Class;', java, 8)).
hatS('Ljava/lang/Double;', 'TYPE:Ljava/lang/Class;', ('Ljava/lang/Class;', java, 9)).
hatH(('Ljava/lang/Class;', java, 1), 'name', 'int').
hatH(('Ljava/lang/Class;', java, 2), 'name', 'boolean').
hatH(('Ljava/lang/Class;', java, 3), 'name', 'void').
hatH(('Ljava/lang/Class;', java, 4), 'name', 'byte').
hatH(('Ljava/lang/Class;', java, 5), 'name', 'short').
hatH(('Ljava/lang/Class;', java, 6), 'name', 'char').
hatH(('Ljava/lang/Class;', java, 7), 'name', 'long').
hatH(('Ljava/lang/Class;', java, 8), 'name', 'float').
hatH(('Ljava/lang/Class;', java, 9), 'name', 'double').


%%% Reflection

degenerateType(('Ljava/lang/Integer;', _, _)).
degenerateType(('Ljava/lang/Boolean;', _, _)).
degenerateType(('Ljava/lang/Character;', _, _)).
degenerateType(('Ljava/lang/Short;', _, _)).
degenerateType(('Ljava/lang/Byte;', _, _)).
degenerateType(('Ljava/lang/Long;', _, _)).
degenerateType(('Ljava/lang/Float;', _, _)).
degenerateType(('Ljava/lang/Double;', _, _)).

invoke(MID, V, Y) :- reflectInvoke(MID, V, Type), degenerateType(Type),
			Type = (CL, AddrMI, AddrPCI), 
			hatH((CL, AddrMI, AddrPCI), 'value', Y).

invoke(MID, V, Y) :- reflectInvoke(MID, V, Type), tnot(degenerateType(Type)), Y = Type.

reflectInvoke(_, _, _) :- fail.
		

%%% Analysis stuff

hatS(_, _, bot_static).

kind(virtual).
kind(static).
kind(direct).

binop(_, _, _, top_prim).
unop(_, _, top_prim).


resolve(CL, MName, ArgTypes, ReturnType, MID) :-
	(resolveFact(CL, MName, ArgTypes, ReturnType, MID), !) ; MID = 'UNRESOLVED method call'.
%resolve(CL, MName, ArgTypes, ReturnType, Kind, MID) :-
%	\+ resolveFact(CL, MName, ArgTypes, ReturnType, Kind, _),
%	MID = 'UNRESOLVED method call'.

method('UNRESOLVED method call', _, _, _, _, _, 0).
hatH((ArgCL, ArgCLAddrMID, ArgCLAddrPC), _, (top_ref, analysis, 0)) :-
        invoke('UNRESOLVED method call', _, (ArgCL, ArgCLAddrMID, ArgCLAddrPC)),
	ArgCL \= 'Ljava/lang/String;'.
return('UNRESOLVED method call', (top_ref, analysis, 0)).

% The conditions for ancestor are carefully ordered such that Prolog resolves from the bottom of class hierarchy and up
ancestor(A, B) :-
	super(A, B).
ancestor(A, B) :-
	super(X, B),
	ancestor(A, X).

subclass(A, A).
subclass(A, B) :-
	ancestor(B, A).

methodsignature(MethodSignature) :-
	MethodSignature = methodsignature(FullName, ClassOrInterface, Name, ArgTypes, ReturnType),
	methodsignature(FullName, ClassOrInterface, Name, ArgTypes, ReturnType).
	% ( class(ClassOrInterface) ; interface(ClassOrInterface) ).  % This should already be checked by the bytecode verifier

method(Method) :-
	Method = method(FullName, Class, Name, ArgTypes, ReturnType, Kind, NumLocals),
	method(FullName, Class, Name, ArgTypes, ReturnType, Kind, NumLocals).
	% class(Class),
	% kind(Kind).

compatible(MethodSignature, Method) :-
	methodsignature(MethodSignature),
	method(Method),
	MethodSignature = methodsignature(_, _, Name, ArgTypes, ReturnType),
	Method = method(_, _, Name, ArgTypes, ReturnType, _, _).

%resolveMethod(Kind, MethodSignature, CL, Head) :-
%	findall(Method, inner_resolveMethod(Kind, MethodSignature, CL, Method), [Head|_]).
%	% inner_resolveMethod(Kind, MethodSignature, CL, Method),
%	% !.
%inner_resolveMethod(Kind, MethodSignature, CL, Method) :-
%	Method = method(_, CL, _, _, _, Kind, _),
%	class(CL),
%	compatible(MethodSignature, Method).
%inner_resolveMethod(Kind, MethodSignature, CL, Method) :-
%	Kind \= direct,
%	super(P, CL),
%	inner_resolveMethod(Kind, MethodSignature, P, Method).

objref(X) :-
	class(X).  % TODO: Represent null
objref(top_ref).  % To allow invoking methods on objects from factory methods
arrref(top_ref).  % To allow invoking methods on objects from factory methods

ref(X) :-
	objref(X) ;
	arrref(X).

handler(_, _, _, _, _, _) :- fail.

canHandle(MID, Index, PC, ExceptionType) :-
	handler(MID, Index, CatchType, Start, End, _),
	Start =< PC,
	PC =< End,
	(
		subclass(ExceptionType, CatchType) ;
		CatchType = top_exception
	).

isFirstHandler(MID, Index, PC, ExceptionType) :-
	canHandle(MID, Index, PC, ExceptionType),
	forall((handler(MID, J, _, _, _, _), J < Index), \+ (canHandle(MID, J, PC, ExceptionType))).

% ExceptionType should be instantiated to get correct results
findHandler(MID, PC, ExceptionType, HandlerPC) :-
	handler(MID, Index, _, _, _, HandlerPC),
	isFirstHandler(MID, Index, PC, ExceptionType),
	!.
findHandler(_, _, _, bot).





%%% Output related rules

% <Implementations copied from SWI>
numlist(A, B, C) :-
	A=<B,
	numlist_(A, B, C).

numlist_(A, A, B) :- !,
	B=[A].
numlist_(A, C, [A|D]) :-
	B is A+1,
	numlist_(B, C, D).

:- meta_predicate ignore(0).

ignore(A) :-
	call(A), !.
ignore(_).
% </Implementations copied from SWI>



printMethodCalls :- forall(
	methodCall(Caller, Callee, CallType),
	(
		((CallType = regular, Style = '') ; (CallType = reflection, Style = '[style = dashed]')),
		write('\"'),
		write(Caller),
		write('\" -> \"'),
		write(Callee),
		write('\" '),
		write(Style),
		writeln(' ;')
	)).

methodContent(MID) :-
	methodLength(MID, MaxPC),
	methodContent(MID, MaxPC).

methodContent(MID, MaxPC) :-
	method(MID, _, _, _, _, _, NumLocals),
	write('NumLocals is '), writeln(NumLocals),
        forall(
                (numlist(0,MaxPC,L),member(PC,L)),
                (
                   ignore((
                            write('\nPC '),
                            write(PC),
                            writeln(':'),
                            registerContent(MID, PC)
                          ))
                ) ).


registerContent(MID, PC) :-
        method(MID, _, _, Args, _, _, NumLocals),
        length(Args, ArgLen),
        RegCount is ArgLen + ArgLen + NumLocals,
        % write('RegCount for the method is: '), writeln(RegCount), %TODO Substract one if the method is static?
        smaliLine(MID, PC, SmaliLine),
        writeln(SmaliLine),
        forall(
                (numlist(0,RegCount,L),member(V,L)),
                (
                   ignore((
                            setof(Y, hatR(MID, PC, V, Y), Ys),
                            write('v'),
                            write(V),
                            write(' = '),
                            writeln(Ys)
                          ))
                ) ),
   ignore((
	    setof(Y, hatR(MID, PC, retval, Y), Ys),
	    write('retval'),
	    write(' = '),
	    writeln(Ys)
	  )).

printStrings :-
	forall((setof(Val, hatH(('Ljava/lang/String;', AddrMID, AddrPC), value, Val), Set), member(M, Set)), writeln((AddrMID, AddrPC, M))).

printStats :- call((
                        write('Tablespace (Alloc, Used): '), statistics(tablespace, Result), writeln(Result),
                        write('Combined heap and local stack (Alloc, Used): '), statistics(gl, Result2), writeln(Result2),
                        write('Combined trail and choise point stack (Alloc, Used): '), statistics(tc, Result3), writeln(Result3),
                        write('Heap: '), statistics(heap, Result4), writeln(Result4),
                        write('Local stack: '), statistics(local, Result5), writeln(Result5),
                        write('Trail stack: '), statistics(trail, Result6), writeln(Result6),
                        write('Choice point stack: '), statistics(choice_point, Result7), writeln(Result7),
                        write('Uncompleted tables in comletion stack: '), statistics(open_tables, Result8), writeln(Result8),
                        write('Atoms in the atom table: '), statistics(atoms, Result9), writeln(Result9),
                        statistics(atom),
                        statistics
                )).

printReflection :-
			writeln('\nClass.forName:'),
			forall(
				setof(Y, reflection('Class.forName', _, _, Y), Set),
				call((write('\t'), writeln(Set)))
			),

			writeln('\nclazz.getMethod.clazzName:'),
			forall(
				setof(Y, reflection('clazz.getMethod.clazzName', _, _, Y), Set),
				call((write('\t'), writeln(Set)))
			),

			writeln('\nclazz.getMethod.methodName:'),
			forall(
				setof(Y, reflection('clazz.getMethod.methodName', _, _, Y), Set),
				call((write('\t'), writeln(Set)))
			),

			true.
