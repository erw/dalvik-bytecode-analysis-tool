#!/bin/bash
date

basename="callgraph"
prolog_script_name="o.pl"

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

time $DIR/generator.py . --nowarn > ${prolog_script_name}

echo 'digraph name { node [label="\N", fontname=Monospace, fontsize=8, shape=plaintext];' > ${basename}.dot

time nice -n 19 xsb -S --noprompt -e "['${prolog_script_name}'], printMethodCalls, printStats, halt." \
	>> ${basename}.dot
	# | grep -v '^"' \
